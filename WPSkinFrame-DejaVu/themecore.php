<?php
/**
 * The BackStopThemes class. Defines the necessary constants 
 * and includes the necessary files for theme's operation.
 *
 * @package BackStopThemes
 * @subpackage Dejavu
 */

function bst_get_theme_name() {
	return 'dejavu';
}
function bst_get_theme_version() {
	return '2.8.8';
}

class BackStopThemes extends BackStopThemesCommon {

	public function load_theme_specific_constants( $options ) {
	}

	/**
	 * Loads theme actions.
	 *
	 * @since 1.0
	 */
	public function load_theme_actions() {
		
		# WordPress actions	
		add_action( 'init', 'mysite_is_mobile_device' );
		add_action( 'init', 'mysite_is_responsive' );
		add_action( 'init', 'mysite_shortcodes_init' );
		add_action( 'init', 'mysite_menus' );
		add_action( 'init', 'mysite_post_types'  );
		add_action( 'init', 'mysite_register_script' );
		add_action( 'init', 'mysite_wp_image_resize', 11 );
		add_action( 'init', array( 'mysiteForm', 'init'), 11 );
		add_action( 'widgets_init', 'mysite_sidebars' );
		add_action( 'widgets_init', 'mysite_widgets' );
		add_action( 'wp_head', 'mysite_seo_meta' );
		add_action( 'wp_head', 'mysite_mobile_meta' );
		add_action( 'wp_head', 'mysite_fancy_search_script' );
		add_action( 'wp_head', 'mysite_analytics' );
		add_action( 'wp_head', 'mysite_custom_bg' );
		add_action( 'wp_head', 'mysite_additional_headers', 99 );
		add_action( 'wp_head', 'mysite_fitvids' );
		add_action( 'template_redirect', 'mysite_enqueue_script' );
		add_action( 'template_redirect', 'mysite_squeeze_page' );
		add_action( 'comment_form_defaults', 'mysite_comment_form_args' );
		remove_action( 'wp_head', 'rel_canonical' );
		
		# BackStopThemes actions
		add_action( 'mysite_head', 'mysite_header_scripts' );
		add_action( 'mysite_before_header', 'mysite_fullscreen_bg' );
		add_action( 'mysite_header', 'mysite_logo' );
		add_action( 'mysite_header', 'mysite_header_extras' );
		add_action( 'mysite_after_header', 'mysite_primary_menu' );
		add_action( 'mysite_after_header', 'mysite_responsive_menu' );
		add_action( 'mysite_after_header', 'mysite_slider_module' );
		add_action( 'mysite_after_header', 'mysite_teaser' );
		add_action( 'mysite_before_page_content', 'mysite_home_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_title' );
		add_action( 'mysite_before_page_content', 'mysite_query_posts' );
		add_action( 'mysite_primary_menu_end', 'mysite_fancy_search' );
		add_action( 'mysite_intro_end', 'mysite_breadcrumbs' );
		add_action( 'mysite_before_post', 'mysite_post_image' );
		add_action( 'mysite_before_entry', 'mysite_post_title' );
		add_action( 'mysite_before_entry', 'mysite_comment_bubble' );
		add_action( 'mysite_singular-portfolio_before_entry', 'mysite_portfolio_date' );
		add_action( 'mysite_singular-post_before_entry', 'mysite_post_meta' );
		add_action( 'mysite_singular-page_before_entry', 'mysite_post_image' );
		add_action( 'mysite_blog_after_entry', 'mysite_blog_meta_bottom' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_nav' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_meta_bottom' );
		add_action( 'mysite_singular-post_after_post', 'mysite_about_author' );
		add_action( 'mysite_singular-post_after_post', 'mysite_like_module' );
		add_action( 'mysite_singular-post_after_post', 'mysite_post_sociables' );
		add_action( 'mysite_singular-portfolio_after_post', 'mysite_post_sociables' );
		add_action( 'mysite_after_post', 'mysite_page_navi' );
		add_action( 'mysite_after_main', 'mysite_get_sidebar' );
		add_action( 'mysite_before_footer', 'mysite_footer_teaser' );
		add_action( 'mysite_footer', 'mysite_main_footer' );
		add_action( 'mysite_after_footer', 'mysite_sub_footer' );
		add_action( 'mysite_body_end', 'mysite_print_cufon' );
		add_action( 'mysite_body_end', 'mysite_image_preloading' );
		add_action( 'mysite_body_end', 'mysite_ios_rotate' );
		add_action( 'mysite_body_end', 'mysite_custom_javascript' );
	}
	
	/**
	 * Loads theme filters.
	 *
	 * @since 1.0
	 */
	public function load_theme_filters() {
		
		# BackStopThemes filters
		add_filter( 'mysite_avatar_size', function(){return "60";} );
		add_filter( 'mysite_author_avatar_size', function(){return "60";} );
		add_filter( 'mysite_additional_posts_title', function(){return;} );
		add_filter( 'mysite_read_more', 'mysite_read_more' );
		add_filter( 'mysite_fancy_link', 'mysite_fancy_link', 1, 2 );
		add_filter( 'mysite_portfolio_read_more', 'mysite_portfolio_read_more', 1, 2 );
		add_filter( 'mysite_portfolio_visit_site', 'mysite_portfolio_visit_site', 1, 2 );
		add_filter( 'mysite_widget_meta', 'mysite_widget_meta' );
		add_filter( 'the_content_more_link', 'mysite_full_read_more', 10, 2 );
		add_filter( 'excerpt_length', 'mysite_excerpt_length_long', 999 );
		add_filter( 'excerpt_more', 'mysite_excerpt_more' );
		add_filter( 'posts_where', 'mysite_multi_tax_terms' );
		add_filter( 'pre_get_posts', 'mysite_exclude_category_feed' );
		add_filter( 'pre_get_posts', 'mysite_custom_search' );
		add_filter( 'widget_categories_args', 'mysite_exclude_category_widget' );
		add_filter( 'query_vars', 'mysite_queryvars' );
		add_filter( 'rewrite_rules_array', 'mysite_rewrite_rules',10,2 );
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'wp_page_menu_args', 'mysite_page_menu_args' );
		add_filter( 'the_password_form', 'mysite_password_form' );
	}
	
	/**
	 * Define theme variables.
	 *
	 * @since 1.0
	 */
	public function load_theme_variables() {
		global $mysite;
		
		$layout = '';
		$img_set = get_option( MYSITE_SETTINGS );
		$img_set = ( !empty( $img_set ) && !isset( $_POST[MYSITE_SETTINGS]['reset'] ) ) ? $img_set : array();
		$blog_layout = apply_filters( 'mysite_blog_layout', mysite_get_setting( 'blog_layout' ) );
		
		# Images
		$images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_full']['w'] ) ? $img_set['one_column_portfolio_full']['w'] : 886 ),
		        ( !empty( $img_set['one_column_portfolio_full']['h'] ) ? $img_set['one_column_portfolio_full']['h'] : 550 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_full']['w'] ) ? $img_set['two_column_portfolio_full']['w'] : 418 ),
		        ( !empty( $img_set['two_column_portfolio_full']['h'] ) ? $img_set['two_column_portfolio_full']['h'] : 259 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_full']['w'] ) ? $img_set['three_column_portfolio_full']['w'] : 262 ),
		        ( !empty( $img_set['three_column_portfolio_full']['h'] ) ? $img_set['three_column_portfolio_full']['h'] : 162 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_full']['w'] ) ? $img_set['four_column_portfolio_full']['w'] : 184 ),
		        ( !empty( $img_set['four_column_portfolio_full']['h'] ) ? $img_set['four_column_portfolio_full']['h'] : 114 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_full']['w'] ) ? $img_set['one_column_blog_full']['w'] : 886 ),
		        ( !empty( $img_set['one_column_blog_full']['h'] ) ? $img_set['one_column_blog_full']['h'] : 375 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_full']['w'] ) ? $img_set['two_column_blog_full']['w'] : 418 ),
		        ( !empty( $img_set['two_column_blog_full']['h'] ) ? $img_set['two_column_blog_full']['h'] : 177 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_full']['w'] ) ? $img_set['three_column_blog_full']['w'] : 262 ),
		        ( !empty( $img_set['three_column_blog_full']['h'] ) ? $img_set['three_column_blog_full']['h'] : 111 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_full']['w'] ) ? $img_set['four_column_blog_full']['w'] : 184 ),
		        ( !empty( $img_set['four_column_blog_full']['h'] ) ? $img_set['four_column_blog_full']['h'] : 77 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_full']['w'] ) ? $img_set['small_post_list_full']['w'] : 50 ),
		        ( !empty( $img_set['small_post_list_full']['h'] ) ? $img_set['small_post_list_full']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_full']['w'] ) ? $img_set['medium_post_list_full']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_full']['h'] ) ? $img_set['medium_post_list_full']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_full']['w'] ) ? $img_set['large_post_list_full']['w'] : 574 ),
		        ( !empty( $img_set['large_post_list_full']['h'] ) ? $img_set['large_post_list_full']['h'] : 358 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_full']['w'] ) ? $img_set['portfolio_single_full_full']['w'] : 980 ),
		        ( !empty( $img_set['portfolio_single_full_full']['h'] ) ? $img_set['portfolio_single_full_full']['h'] : 600 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_full']['w'] ) ? $img_set['additional_posts_grid_full']['w'] : 184 ),
		        ( !empty( $img_set['additional_posts_grid_full']['h'] ) ? $img_set['additional_posts_grid_full']['h'] : 114 )),

		);

		$big_sidebar_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_big']['w'] ) ? $img_set['one_column_portfolio_big']['w'] : 566 ),
		        ( !empty( $img_set['one_column_portfolio_big']['h'] ) ? $img_set['one_column_portfolio_big']['h'] : 351 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_big']['w'] ) ? $img_set['two_column_portfolio_big']['w'] : 264 ),
		        ( !empty( $img_set['two_column_portfolio_big']['h'] ) ? $img_set['two_column_portfolio_big']['h'] : 163 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_big']['w'] ) ? $img_set['three_column_portfolio_big']['w'] : 163 ),
		        ( !empty( $img_set['three_column_portfolio_big']['h'] ) ? $img_set['three_column_portfolio_big']['h'] : 101 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_big']['w'] ) ? $img_set['four_column_portfolio_big']['w'] : 113 ),
		        ( !empty( $img_set['four_column_portfolio_big']['h'] ) ? $img_set['four_column_portfolio_big']['h'] : 70 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_big']['w'] ) ? $img_set['one_column_blog_big']['w'] : 566 ),
		        ( !empty( $img_set['one_column_blog_big']['h'] ) ? $img_set['one_column_blog_big']['h'] : 239 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_big']['w'] ) ? $img_set['two_column_blog_big']['w'] : 264 ),
		        ( !empty( $img_set['two_column_blog_big']['h'] ) ? $img_set['two_column_blog_big']['h'] : 111 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_big']['w'] ) ? $img_set['three_column_blog_big']['w'] : 163 ),
		        ( !empty( $img_set['three_column_blog_big']['h'] ) ? $img_set['three_column_blog_big']['h'] : 69 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_big']['w'] ) ? $img_set['four_column_blog_big']['w'] : 113 ),
		        ( !empty( $img_set['four_column_blog_big']['h'] ) ? $img_set['four_column_blog_big']['h'] : 47 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_big']['w'] ) ? $img_set['small_post_list_big']['w'] : 50 ),
		        ( !empty( $img_set['small_post_list_big']['h'] ) ? $img_set['small_post_list_big']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_big']['w'] ) ? $img_set['medium_post_list_big']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_big']['h'] ) ? $img_set['medium_post_list_big']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_big']['w'] ) ? $img_set['large_post_list_big']['w'] : 364 ),
		        ( !empty( $img_set['large_post_list_big']['h'] ) ? $img_set['large_post_list_big']['h'] : 227 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_big']['w'] ) ? $img_set['portfolio_single_full_big']['w'] : 566 ),
		        ( !empty( $img_set['portfolio_single_full_big']['h'] ) ? $img_set['portfolio_single_full_big']['h'] : 351 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_big']['w'] ) ? $img_set['additional_posts_grid_big']['w'] : 113 ),
		        ( !empty( $img_set['additional_posts_grid_big']['h'] ) ? $img_set['additional_posts_grid_big']['h'] : 70 )),

		);

		$small_sidebar_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_small']['w'] ) ? $img_set['one_column_portfolio_small']['w'] : 641 ),
		        ( !empty( $img_set['one_column_portfolio_small']['h'] ) ? $img_set['one_column_portfolio_small']['h'] : 398 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_small']['w'] ) ? $img_set['two_column_portfolio_small']['w'] : 300 ),
		        ( !empty( $img_set['two_column_portfolio_small']['h'] ) ? $img_set['two_column_portfolio_small']['h'] : 186 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_small']['w'] ) ? $img_set['three_column_portfolio_small']['w'] : 186 ),
		        ( !empty( $img_set['three_column_portfolio_small']['h'] ) ? $img_set['three_column_portfolio_small']['h'] : 115 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_small']['w'] ) ? $img_set['four_column_portfolio_small']['w'] : 130 ),
		        ( !empty( $img_set['four_column_portfolio_small']['h'] ) ? $img_set['four_column_portfolio_small']['h'] : 80 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_small']['w'] ) ? $img_set['one_column_blog_small']['w'] : 641 ),
		        ( !empty( $img_set['one_column_blog_small']['h'] ) ? $img_set['one_column_blog_small']['h'] : 271 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_small']['w'] ) ? $img_set['two_column_blog_small']['w'] : 300 ),
		        ( !empty( $img_set['two_column_blog_small']['h'] ) ? $img_set['two_column_blog_small']['h'] : 127 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_small']['w'] ) ? $img_set['three_column_blog_small']['w'] : 186 ),
		        ( !empty( $img_set['three_column_blog_small']['h'] ) ? $img_set['three_column_blog_small']['h'] : 78 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_small']['w'] ) ? $img_set['four_column_blog_small']['w'] : 130 ),
		        ( !empty( $img_set['four_column_blog_small']['h'] ) ? $img_set['four_column_blog_small']['h'] : 55 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_small']['w'] ) ? $img_set['small_post_list_small']['w'] : 50 ),
		        ( !empty( $img_set['small_post_list_small']['h'] ) ? $img_set['small_post_list_small']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_small']['w'] ) ? $img_set['medium_post_list_small']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_small']['h'] ) ? $img_set['medium_post_list_small']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_small']['w'] ) ? $img_set['large_post_list_small']['w'] : 413 ),
		        ( !empty( $img_set['large_post_list_small']['h'] ) ? $img_set['large_post_list_small']['h'] : 258 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_small']['w'] ) ? $img_set['portfolio_single_full_small']['w'] : 641 ),
		        ( !empty( $img_set['portfolio_single_full_small']['h'] ) ? $img_set['portfolio_single_full_small']['h'] : 398 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_small']['w'] ) ? $img_set['additional_posts_grid_small']['w'] : 130 ),
		        ( !empty( $img_set['additional_posts_grid_small']['h'] ) ? $img_set['additional_posts_grid_small']['h'] : 80 )),

		);
		
		$additional_images = array(
		    'image_banner_intro' => array( 
		        ( !empty( $img_set['image_banner_intro_full']['w'] ) ? $img_set['image_banner_intro_full']['w'] : 980 ),
		        ( !empty( $img_set['image_banner_intro_full']['h'] ) ? $img_set['image_banner_intro_full']['h'] : 460 )),
		);



		# Slider
		$images_slider = array(
			'responsive_slide' => array( 972, 460 ),
			'floating_slide' => array( 900, 350 ),
			'staged_slide' => array( 900, 350 ),
			'partial_staged_slide' => array( 567, 334 ),
			'partial_gradient_slide' => array(500, 350),
			'overlay_slide' => array(900, 350),
			'full_slide' => array( 980, 460 ),
			'nivo_slide' => array( 980, 500 ),
			'nav_thumbs' => array( 50, 40 )
		);
		
		foreach( (array) $images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$images[$key] = $new_size;
		}

		foreach( (array) $big_sidebar_images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$big_sidebar_images[$key] = $new_size;
		}

		foreach( (array) $small_sidebar_images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$small_sidebar_images[$key] = $new_size;
		}
		
		foreach( (array) $additional_images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$additional_images[$key] = $new_size;
		}
		
		# Blog layouts
		switch( $blog_layout ) {
			case "blog_layout1":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout1',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image'
				);
				break;
			case "blog_layout2":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_list blog_layout2',
					'post_class' => 'post_list_module',
					'content_class' => 'post_list_content',
					'img_class' => 'post_list_image'
				);
				break;
			case "blog_layout3":
				$columns_num = 2;
				$featured = 1;
				$columns = ( $columns_num == 2 ? 'one_half'
				: ( $columns_num == 3 ? 'one_third'
				: ( $columns_num == 4 ? 'one_fourth'
				: ( $columns_num == 5 ? 'one_fifth'
				: ( $columns_num == 6 ? 'one_sixth'
				: ''
				)))));

				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout3',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image',
					'columns_num' => ( !empty( $columns_num ) ? $columns_num : '' ),
					'featured' => ( !empty( $featured ) ? $featured : '' ),
					'columns' => ( !empty( $columns ) ? $columns : '' )
				);
				break;
		}

		$mysite->layout['blog'] = $layout;
		$mysite->layout['images'] = array_merge( $images, array( 'image_padding' => 14 ) );
		$mysite->layout['big_sidebar_images'] = $big_sidebar_images;
		$mysite->layout['small_sidebar_images'] = $small_sidebar_images;
		$mysite->layout['additional_images'] = $additional_images;
		$mysite->layout['images_slider'] = $images_slider;
	}
				
}

/**
 * Functions & Pluggable functions specific to theme.
 *
 * @package BackStopThemes
 * @subpackage Dejavu
 */

if ( !function_exists( 'mysite_read_more' ) ) :
/**
 *
 */
function mysite_read_more( $url ) {
	return '<span class="post_more_link"><a class="post_more_link_a" href="' . $url . '">' . __( 'Read More', 'backstop-themes' ) . '</a><span class="post_more_link_arrow"></span></span>';
}
endif;

if ( !function_exists( 'mysite_portfolio_read_more' ) ) :
/**
 *
 */
function mysite_portfolio_read_more( $read_more, $url ) {
	return '<span class="post_more_link"><a href="' . $url  . '" class="post_more_link_a">' . __( 'Read More', 'backstop-themes' ) . '</a><span class="post_more_link_arrow"></span></span>';
}
endif;

if ( !function_exists( 'mysite_portfolio_visit_site' ) ) :
/**
 *
 */
function mysite_portfolio_visit_site( $visit_site, $url ) {
	return '<span class="post_more_link"><a href="' . $url  . '" class="post_more_link_a">' . __( 'Visit Site', 'backstop-themes' ) . '</a><span class="post_more_link_arrow"></span></span>';
}
endif;

if ( !function_exists( 'mysite_fancy_link' ) ) :
/**
 *
 */
function mysite_fancy_link( $fancy_link, $args ) {
	extract( $args );
	return '<span class="fancy_link"><a href="' . esc_url( $link ) . '" class="fancy_link_a' . $target . $variation . '"' . $color .'>' . mysite_remove_wpautop( $content ) . '</a><span class="fancy_link_arrow"></span></span>';
}
endif;

if ( !function_exists( 'mysite_post_meta' ) ) :
/**
 *
 */
function mysite_post_meta( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	extract( $args );
	
	if( is_page() && !$shortcode ) return;
	
	$out = '';
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';
	
	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_date] ';
		
	if( !in_array( 'author_meta', $_meta ) )
		$meta_output .= '[post_author text="' . __( '<em>by:</em>', 'backstop-themes' ) . ' "] ';
	
	if( !empty( $meta_output ) )
		$out .='<p class="post_meta">' . $meta_output . '</p>';
	
	if( $echo )
		echo apply_atomic_shortcode( 'post_meta', $out );
	else
		return apply_atomic_shortcode( 'post_meta', $out );
}
endif;

if ( !function_exists( 'mysite_blog_meta_bottom' ) ) :
/**
 *
 */
function mysite_blog_meta_bottom( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	extract( $args );
	
	if ( is_page() && !$shortcode ) return;
	
	$out = '';
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';

	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_date] ';

	if( !in_array( 'categories_meta', $_meta ) )
		$meta_output .= '[post_terms taxonomy="category"] ';

	if( !empty( $meta_output ) )
		$out .='<p class="post_meta_bottom">' . $meta_output . '</p>';
	
	if( $echo )
		echo apply_atomic_shortcode( 'blog_meta_bottom', $out );
	else
		return apply_atomic_shortcode( 'blog_meta_bottom', $out );
}
endif;

if ( !function_exists( 'mysite_before_post_sc' ) ) :
/**
 *
 */
function mysite_before_post_sc( $filter_args ) {
	$out = '';
	
	if( strpos( $filter_args['disable'], 'image' ) === false )
		$out .= mysite_get_post_image( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_before_entry_sc' ) ) :
/**
 *
 */
function mysite_before_entry_sc( $filter_args ) {
	$out = '';
	
	if( strpos( $filter_args['disable'], 'title' ) === false )
		$out .= mysite_post_title( $filter_args );
	
	if( strpos( $filter_args['disable'], 'meta' ) === false )
		$out .= mysite_comment_bubble( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_after_entry_sc' ) ) :
/**
 *
 */
function mysite_after_entry_sc( $filter_args ) {
	$out = '';
	
	if( strpos( $filter_args['disable'], 'meta' ) === false )
		$out .= mysite_blog_meta_bottom( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_widget_meta' ) ) :
/**
 *
 */
function mysite_widget_meta() {
	return do_shortcode( '[post_date text=""]' );
}
endif;

if ( !function_exists( 'mysite_comment_bubble' ) ) :
/**
 *
 */
function mysite_comment_bubble( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$number = get_comments_number();
	if( ( $number<1 && !comments_open() ) || ( is_attachment() ) || ( is_singular( 'portfolio' ) ) ) return;
	
	$args = wp_parse_args( $args, $defaults );
	extract( $args );
	
	if ( is_page() && !$shortcode ) return;
	
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	if( is_array( $meta_options ) && in_array( 'comments_meta', $meta_options ) )
		return;
	
	$out = '<div class="post_comments_bubble">[post_comments text="" more="%1$s" one="1" zero="0"]</div>';
	
	if( $echo )
		echo apply_atomic_shortcode( 'comment_bubble', $out );
	else
		return apply_atomic_shortcode( 'comment_bubble', $out );
}
endif;

if ( !function_exists( 'mysite_fancy_search' ) ) :
/**
 * 
 */
function mysite_fancy_search() {
	
	$out = '<div id="fancy_search" class="search_hidden">';
	$out .= '<form id = "searchform" method="get" action="' . get_bloginfo( 'url' ) . '/">';
	$out .= '<input class="text" name="s" id="s" style="width:0px;margin:0;display:none;" />';
	$out .= '<input type="submit" value="submit" class="submit" />';
	if ( get_query_var( 'lang' ) ) { echo '<input type = "hidden" name = "lang" value = "'.get_query_var('lang').'" />'; }
	$out .= '</form>';
	$out .= '</div>';
	
	echo apply_atomic( 'fancy_search', $out );
}
endif;

if ( !function_exists( 'mysite_fancy_search_script' ) ) :
/**
 *
 */
function mysite_fancy_search_script() {
	
	$out = "<script type=\"text/javascript\">
	/* <![CDATA[ */
		jQuery(document).ready(function() {
			var search = jQuery('#fancy_search'),
			    searchField = search.find('input.text');

			var hoverConfigSearch = {
				interval:100,
				timeout:2000,
				over:function(){
					jQuery(searchField).css('display', 'block');
					searchField.animate({width:130}, 400, 'easeInOutQuad').focus();
				},
				out:function(){
					searchField.animate({width:20}, 400, 'easeInOutQuad', function(){ jQuery(searchField).css('display', 'none') } ).blur();
				}
			};
			search.hoverIntent(hoverConfigSearch);
	});
	/* ]]> */
	</script>";
	
	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out );
}
endif;

?>