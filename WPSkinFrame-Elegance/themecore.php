<?php
/**
 * The BackStopThemes class. Defines the necessary constants 
 * and includes the necessary files for theme's operation.
 *
 * @package BackStopThemes
 * @subpackage Elegance
 */

function bst_get_theme_name() {
	return 'elegance';
}
function bst_get_theme_version() {
	return '2.8.8';
}

class BackStopThemes extends BackStopThemesCommon {

	public function load_theme_specific_constants( $options ) {
	}
	
	/**
	 * Loads theme actions.
	 *
	 * @since 1.0
	 */
	public function load_theme_actions() {
		
		# WordPress actions
		add_action( 'init', 'mysite_is_mobile_device' );
		add_action( 'init', 'mysite_is_responsive' );
		add_action( 'init', 'mysite_shortcodes_init' );
		add_action( 'init', 'mysite_menus' );
		add_action( 'init', 'mysite_post_types'  );
		add_action( 'init', 'mysite_register_script' );
		add_action( 'init', 'mysite_wp_image_resize', 11 );
		add_action( 'init', array( 'mysiteForm', 'init'), 11 );
		add_action( 'widgets_init', 'mysite_sidebars' );
		add_action( 'widgets_init', 'mysite_widgets' );
		add_action( 'wp_head', 'mysite_seo_meta' );
		add_action( 'wp_head', 'mysite_mobile_meta' );
		add_action( 'wp_head', 'mysite_fancy_search_script' );
		add_action( 'wp_head', 'mysite_analytics' );
		add_action( 'wp_head', 'mysite_custom_bg' );
		add_action( 'wp_head', 'mysite_additional_headers', 99 );
		add_action( 'wp_head', 'mysite_fitvids' );
		add_action( 'template_redirect', 'mysite_enqueue_script' );
		add_action( 'template_redirect', 'mysite_squeeze_page' );
		add_action( 'comment_form_defaults', 'mysite_comment_form_args' );
		remove_action( 'wp_head', 'rel_canonical' );
		
		# BackStopThemes actions
		add_action( 'mysite_head', 'mysite_header_scripts' );
		add_action( 'mysite_before_header', 'mysite_fullscreen_bg' );
		add_action( 'mysite_header', 'mysite_logo' );
		add_action( 'mysite_header', 'mysite_header_extras' );
		add_action( "mysite_after_header", 'mysite_primary_menu' );
		add_action( 'mysite_after_header', 'mysite_responsive_menu' );
		add_action( 'mysite_after_header', 'mysite_slider_module' );
		add_action( 'mysite_after_header', 'mysite_teaser' );
		add_action( 'mysite_after_header', 'mysite_breadcrumbs' );
		add_action( 'mysite_before_page_content', 'mysite_home_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_title' );
		add_action( 'mysite_before_page_content', 'mysite_query_posts' );
		add_action( 'mysite_primary_menu_end', 'mysite_fancy_search' );
		add_action( 'mysite_before_post', 'mysite_post_image' );
		add_action( 'mysite_portfolio_image_end', 'mysite_portfolio_meta' );
		add_action( 'mysite_post_image_end', 'mysite_fancy_meta' );
		add_action( 'mysite_before_entry', 'mysite_post_title' );
		add_action( 'mysite_before_entry', 'mysite_comment_bubble' );
		add_action( 'mysite_singular-page_before_entry', 'mysite_post_image' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_meta_bottom' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_nav' );
		add_action( 'mysite_singular-post_after_post', 'mysite_like_module' );
		add_action( 'mysite_singular-post_after_post', 'mysite_post_sociables' );
		add_action( 'mysite_singular-post_after_post', 'mysite_about_author' );
		add_action( 'mysite_singular-portfolio_after_post', 'mysite_post_sociables' );
		add_action( 'mysite_after_post', 'mysite_page_navi' );
		add_action( 'mysite_after_main', 'mysite_get_sidebar' );
		add_action( 'mysite_before_footer', 'mysite_footer_teaser' );
		add_action( 'mysite_footer', 'mysite_main_footer' );
		add_action( 'mysite_after_footer', 'mysite_sub_footer' );
		add_action( 'mysite_body_end', 'mysite_print_cufon' );
		add_action( 'mysite_body_end', 'mysite_image_preloading' );
		add_action( 'mysite_body_end', 'mysite_ios_rotate' );
		add_action( 'mysite_body_end', 'mysite_custom_javascript' );
	}
	
	/**
	 * Loads theme filters.
	 *
	 * @since 1.0
	 */
	public function load_theme_filters() {
		
		# BackStopThemes filters
		add_filter( 'mysite_avatar_size', function(){return "70";} );
		add_filter( 'mysite_read_more', 'mysite_read_more' );
		add_filter( 'mysite_portfolio_read_more', 'mysite_portfolio_read_more', 1, 2 );
		add_filter( 'mysite_portfolio_visit_site', 'mysite_portfolio_visit_site', 1, 2 );
		add_filter( 'mysite_portfolio_date', 'mysite_portfolio_date', 1, 2 );
		add_filter( 'the_content_more_link', 'mysite_full_read_more', 10, 2 );
		add_filter( 'excerpt_length', 'mysite_excerpt_length_long', 999 );
		add_filter( 'excerpt_more', 'mysite_excerpt_more' );
		add_filter( 'posts_where', 'mysite_multi_tax_terms' );
		add_filter( 'pre_get_posts', 'mysite_exclude_category_feed' );
		add_filter( 'pre_get_posts', 'mysite_custom_search' );
		add_filter( 'widget_categories_args', 'mysite_exclude_category_widget' );
		add_filter( 'query_vars', 'mysite_queryvars' );
		add_filter( 'rewrite_rules_array', 'mysite_rewrite_rules',10,2 );
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'wp_page_menu_args', 'mysite_page_menu_args' );
		add_filter( 'the_password_form', 'mysite_password_form' );
	}
	
	/**
	 * Define theme variables.
	 *
	 * @since 1.0
	 */
	public function load_theme_variables() {
		global $mysite;
		
		$layout = '';
		$img_set = get_option( MYSITE_SETTINGS );
		$img_set = ( !empty( $img_set ) && !isset( $_POST[MYSITE_SETTINGS]['reset'] ) ) ? $img_set : array();
		$blog_layout = apply_filters( 'mysite_blog_layout', mysite_get_setting( 'blog_layout' ) );
		
		# Images
		$images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_full']['w'] ) ? $img_set['one_column_portfolio_full']['w'] : 960 ),
		        ( !empty( $img_set['one_column_portfolio_full']['h'] ) ? $img_set['one_column_portfolio_full']['h'] : 596 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_full']['w'] ) ? $img_set['two_column_portfolio_full']['w'] : 460 ),
		        ( !empty( $img_set['two_column_portfolio_full']['h'] ) ? $img_set['two_column_portfolio_full']['h'] : 285 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_full']['w'] ) ? $img_set['three_column_portfolio_full']['w'] : 294 ),
		        ( !empty( $img_set['three_column_portfolio_full']['h'] ) ? $img_set['three_column_portfolio_full']['h'] : 182 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_full']['w'] ) ? $img_set['four_column_portfolio_full']['w'] : 211 ),
		        ( !empty( $img_set['four_column_portfolio_full']['h'] ) ? $img_set['four_column_portfolio_full']['h'] : 131 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_full']['w'] ) ? $img_set['one_column_blog_full']['w'] : 960 ),
		        ( !empty( $img_set['one_column_blog_full']['h'] ) ? $img_set['one_column_blog_full']['h'] : 400 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_full']['w'] ) ? $img_set['two_column_blog_full']['w'] : 460 ),
		        ( !empty( $img_set['two_column_blog_full']['h'] ) ? $img_set['two_column_blog_full']['h'] : 191 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_full']['w'] ) ? $img_set['three_column_blog_full']['w'] : 294 ),
		        ( !empty( $img_set['three_column_blog_full']['h'] ) ? $img_set['three_column_blog_full']['h'] : 122 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_full']['w'] ) ? $img_set['four_column_blog_full']['w'] : 211 ),
		        ( !empty( $img_set['four_column_blog_full']['h'] ) ? $img_set['four_column_blog_full']['h'] : 87 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_full']['w'] ) ? $img_set['small_post_list_full']['w'] : 50 ),
		        ( !empty( $img_set['small_post_list_full']['h'] ) ? $img_set['small_post_list_full']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_full']['w'] ) ? $img_set['medium_post_list_full']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_full']['h'] ) ? $img_set['medium_post_list_full']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_full']['w'] ) ? $img_set['large_post_list_full']['w'] : 627 ),
		        ( !empty( $img_set['large_post_list_full']['h'] ) ? $img_set['large_post_list_full']['h'] : 389 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_full']['w'] ) ? $img_set['portfolio_single_full_full']['w'] : 960 ),
		        ( !empty( $img_set['portfolio_single_full_full']['h'] ) ? $img_set['portfolio_single_full_full']['h'] : 596 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_full']['w'] ) ? $img_set['additional_posts_grid_full']['w'] : 240 ),
		        ( !empty( $img_set['additional_posts_grid_full']['h'] ) ? $img_set['additional_posts_grid_full']['h'] : 150 )),

		);

		$big_sidebar_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_big']['w'] ) ? $img_set['one_column_portfolio_big']['w'] : 660 ),
		        ( !empty( $img_set['one_column_portfolio_big']['h'] ) ? $img_set['one_column_portfolio_big']['h'] : 409 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_big']['w'] ) ? $img_set['two_column_portfolio_big']['w'] : 316 ),
		        ( !empty( $img_set['two_column_portfolio_big']['h'] ) ? $img_set['two_column_portfolio_big']['h'] : 196 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_big']['w'] ) ? $img_set['three_column_portfolio_big']['w'] : 202 ),
		        ( !empty( $img_set['three_column_portfolio_big']['h'] ) ? $img_set['three_column_portfolio_big']['h'] : 125 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_big']['w'] ) ? $img_set['four_column_portfolio_big']['w'] : 145 ),
		        ( !empty( $img_set['four_column_portfolio_big']['h'] ) ? $img_set['four_column_portfolio_big']['h'] : 90 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_big']['w'] ) ? $img_set['one_column_blog_big']['w'] : 660 ),
		        ( !empty( $img_set['one_column_blog_big']['h'] ) ? $img_set['one_column_blog_big']['h'] : 275 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_big']['w'] ) ? $img_set['two_column_blog_big']['w'] : 316 ),
		        ( !empty( $img_set['two_column_blog_big']['h'] ) ? $img_set['two_column_blog_big']['h'] : 131 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_big']['w'] ) ? $img_set['three_column_blog_big']['w'] : 202 ),
		        ( !empty( $img_set['three_column_blog_big']['h'] ) ? $img_set['three_column_blog_big']['h'] : 84 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_big']['w'] ) ? $img_set['four_column_blog_big']['w'] : 145 ),
		        ( !empty( $img_set['four_column_blog_big']['h'] ) ? $img_set['four_column_blog_big']['h'] : 60 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_big']['w'] ) ? $img_set['small_post_list_big']['w'] : 50 ),
		        ( !empty( $img_set['small_post_list_big']['h'] ) ? $img_set['small_post_list_big']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_big']['w'] ) ? $img_set['medium_post_list_big']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_big']['h'] ) ? $img_set['medium_post_list_big']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_big']['w'] ) ? $img_set['large_post_list_big']['w'] : 431 ),
		        ( !empty( $img_set['large_post_list_big']['h'] ) ? $img_set['large_post_list_big']['h'] : 267 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_big']['w'] ) ? $img_set['portfolio_single_full_big']['w'] : 660 ),
		        ( !empty( $img_set['portfolio_single_full_big']['h'] ) ? $img_set['portfolio_single_full_big']['h'] : 409 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_big']['w'] ) ? $img_set['additional_posts_grid_big']['w'] : 165 ),
		        ( !empty( $img_set['additional_posts_grid_big']['h'] ) ? $img_set['additional_posts_grid_big']['h'] : 110 )),

		);

		$small_sidebar_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_small']['w'] ) ? $img_set['one_column_portfolio_small']['w'] : 700 ),
		        ( !empty( $img_set['one_column_portfolio_small']['h'] ) ? $img_set['one_column_portfolio_small']['h'] : 434 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_small']['w'] ) ? $img_set['two_column_portfolio_small']['w'] : 336 ),
		        ( !empty( $img_set['two_column_portfolio_small']['h'] ) ? $img_set['two_column_portfolio_small']['h'] : 208 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_small']['w'] ) ? $img_set['three_column_portfolio_small']['w'] : 214 ),
		        ( !empty( $img_set['three_column_portfolio_small']['h'] ) ? $img_set['three_column_portfolio_small']['h'] : 132 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_small']['w'] ) ? $img_set['four_column_portfolio_small']['w'] : 154 ),
		        ( !empty( $img_set['four_column_portfolio_small']['h'] ) ? $img_set['four_column_portfolio_small']['h'] : 95 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_small']['w'] ) ? $img_set['one_column_blog_small']['w'] : 700 ),
		        ( !empty( $img_set['one_column_blog_small']['h'] ) ? $img_set['one_column_blog_small']['h'] : 291 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_small']['w'] ) ? $img_set['two_column_blog_small']['w'] : 336 ),
		        ( !empty( $img_set['two_column_blog_small']['h'] ) ? $img_set['two_column_blog_small']['h'] : 140 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_small']['w'] ) ? $img_set['three_column_blog_small']['w'] : 214 ),
		        ( !empty( $img_set['three_column_blog_small']['h'] ) ? $img_set['three_column_blog_small']['h'] : 89 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_small']['w'] ) ? $img_set['four_column_blog_small']['w'] : 154 ),
		        ( !empty( $img_set['four_column_blog_small']['h'] ) ? $img_set['four_column_blog_small']['h'] : 64 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_small']['w'] ) ? $img_set['small_post_list_small']['w'] : 50 ),
		        ( !empty( $img_set['small_post_list_small']['h'] ) ? $img_set['small_post_list_small']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_small']['w'] ) ? $img_set['medium_post_list_small']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_small']['h'] ) ? $img_set['medium_post_list_small']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_small']['w'] ) ? $img_set['large_post_list_small']['w'] : 457 ),
		        ( !empty( $img_set['large_post_list_small']['h'] ) ? $img_set['large_post_list_small']['h'] : 283 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_small']['w'] ) ? $img_set['portfolio_single_full_small']['w'] : 700 ),
		        ( !empty( $img_set['portfolio_single_full_small']['h'] ) ? $img_set['portfolio_single_full_small']['h'] : 434 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_small']['w'] ) ? $img_set['additional_posts_grid_small']['w'] : 175 ),
		        ( !empty( $img_set['additional_posts_grid_small']['h'] ) ? $img_set['additional_posts_grid_small']['h'] : 115 )),

		);

		$additional_images = array(
		    'image_banner_intro' => array( 
		        ( !empty( $img_set['image_banner_intro_full']['w'] ) ? $img_set['image_banner_intro_full']['w'] : 960 ),
		        ( !empty( $img_set['image_banner_intro_full']['h'] ) ? $img_set['image_banner_intro_full']['h'] : 460 )),
		);




		# Slider
		$images_slider = array(
			'responsive_slide' => array( 960, 460 ),
			'nivo_slide' => array( 960, 400 ),
			'floating_slide' => array( 960, 400 ),
			'staged_slide' => array( 960, 400 ),
			'partial_staged_slide' => array( 570, 370 ),
			'partial_gradient_slide' => array( 560, 400 ),
			'overlay_slide' => array( 960, 400 ),
			'full_slide' => array( 960, 460 ),
			'nav_thumbs' => array( 34, 34 )
		);
		
		foreach( (array) $images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$images[$key] = $new_size;
		}

		foreach( (array) $big_sidebar_images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$big_sidebar_images[$key] = $new_size;
		}

		foreach( (array) $small_sidebar_images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$small_sidebar_images[$key] = $new_size;
		}
		
		foreach( (array) $additional_images as $key => $value ) {
			foreach( (array) $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$additional_images[$key] = $new_size;
		}
		
		# Blog layouts
		switch( $blog_layout ) {
			case "blog_layout1":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout1',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image'
				);
				break;
			case "blog_layout2":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_list blog_layout2',
					'post_class' => 'post_list_module',
					'content_class' => 'post_list_content',
					'img_class' => 'post_list_image'
				);
				break;
			case "blog_layout3":
				$columns_num = 2;
				$featured = 1;
				$columns = ( $columns_num == 2 ? 'one_half'
				: ( $columns_num == 3 ? 'one_third'
				: ( $columns_num == 4 ? 'one_fourth'
				: ( $columns_num == 5 ? 'one_fifth'
				: ( $columns_num == 6 ? 'one_sixth'
				: ''
				)))));

				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout3',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image',
					'columns_num' => ( !empty( $columns_num ) ? $columns_num : '' ),
					'featured' => ( !empty( $featured ) ? $featured : '' ),
					'columns' => ( !empty( $columns ) ? $columns : '' )
				);
				break;
		}

		$mysite->layout['blog'] = $layout;
		$mysite->layout['images'] = array_merge( $images, array( 'image_padding' => 0 ) );
		$mysite->layout['big_sidebar_images'] = $big_sidebar_images;
		$mysite->layout['small_sidebar_images'] = $small_sidebar_images;
		$mysite->layout['additional_images'] = $additional_images;
		$mysite->layout['images_slider'] = $images_slider;
	}
	
}


/**
 * Functions & Pluggable functions specific to theme.
 *
 * @package BackStopThemes
 * @subpackage Elegance
 */

if ( !function_exists( 'mysite_fancy_meta' ) ) :
/**
 *
 */
function mysite_fancy_meta( $args = array() ) {
	global $mysite;
	
	extract( $args );
	
	$out = '';
	
	$post_id = get_the_ID();
	$type = get_post_type( $post_id );
	$post_thumbnail_id = get_post_thumbnail_id( $post_id );
	$auto_img = mysite_get_setting( 'auto_img' );
	
	$layout = $mysite->layout['blog'];
	$featured = ( isset( $layout['featured'] ) ) ? $layout['featured'] : '';
	$index = ( isset( $index ) ) ? $index : '';
	
	if( !empty( $column ) ) {
		$layout['blog_layout'] = '';
		$featured = '';
		$index = 1;
	}
	
	if ( ( empty( $post_thumbnail_id ) ) && ( !$auto_img[0] ) || ( $layout['blog_layout'] == 'blog_layout2' && !empty( $mysite->is_blog ) ) ) {
		add_action( 'mysite_before_entry', 'mysite_post_meta' );
		return;
	}
		
	if ( ( empty( $post_thumbnail_id ) ) && ( $auto_img[0] ) ) {
		$attachments = get_children(array(
			'post_parent' => $post_id,
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => 'ASC',
			'orderby' => 'menu_order ID',
			'numberposts' => 1
		));
		
		if ( empty( $attachments ) ) {
			add_action( 'mysite_before_entry', 'mysite_post_meta' );
			return;
		}
	}
	
	if( !empty( $shortcode ) && strpos( $disable, 'meta' ) !== false ) return;
	
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';
	
	if( ( !empty( $featured_post ) ) || ( $layout['blog_layout'] == 'blog_layout1' ) || ( $column == 1 ) || ( $thumb == 'large' ) || ( is_single() ) ) {
		if( !in_array( 'author_meta', $_meta ) )
			$meta_output .= '[post_author] ';
			
		if( !in_array( 'date_meta', $_meta ) )
			$meta_output .= '[post_date] ';
			
		if( !in_array( 'categories_meta', $_meta ) )
			$meta_output .= '[post_terms taxonomy="category"]';
			
		if( !empty( $meta_output ) )
			$out .= '<div class="fancy_meta" style="display:none;">' . $meta_output .'</div>';
				
	} elseif( $column != 3 && $column != 4 ) {
		
		if( !in_array( 'author_meta', $_meta ) )
			$meta_output .= '[post_author] ';
			
		if( !in_array( 'date_meta', $_meta ) )
			$meta_output .= '[post_date] ';
			
		if( !empty( $meta_output ) )
			$out .= '<div class="fancy_meta" style="display:none;">' . $meta_output .'</div>';
	}
	
	if( $type == 'portfolio' ) {
		$out = '';
		$_date = get_post_meta( $post_id, '_date', true );
		if( !empty( $_date ) )
			$meta_output = '<span class="meta_date">' . $_date . '</span>';
		else
			$meta_output = '';

		if( !empty( $meta_output ) )
			$out = '<div class="fancy_meta" style="display:none;">' . $meta_output . '</div>';
	}
	
	echo apply_atomic_shortcode( 'fancy_meta', $out );
}
endif;

if ( !function_exists( 'mysite_post_meta_bottom' ) ) :
/**
 *
 */
function mysite_post_meta_bottom() {
	if( is_page() ) return;
	
	$out = '';
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';

	if( !in_array( 'tags_meta', $_meta ) )
		$meta_output .= '[post_terms text=' . __( '<em>Tags:</em>&nbsp;', 'backstop-themes' ) . ']';

	if( !empty( $meta_output ) )
		$out .='<p class="post_meta_bottom">' . $meta_output . '</p>';
	
	echo apply_atomic_shortcode( 'post_meta_bottom', $out );
}
endif;

if ( !function_exists( 'mysite_portfolio_meta' ) ) :
/**
 *
 */
function mysite_portfolio_meta( $filter_args ) {
	$out = '';
	
	extract( $filter_args );
	
	if( !empty( $thumb ) && $thumb != 'large' )
		return;
		
	$meta_output = '';
	
	if( strpos( $disable, 'date_meta' ) === false ) {
		if( !empty( $date ) )
			$meta_output .= '<span class="meta_date">' . $date . '</span>';
	}
		
	if( !empty( $meta_output ) )
		$out .= '<div class="fancy_meta" style="display:none;">' . $meta_output . '</div>';
	
	echo apply_atomic_shortcode( 'portfolio_fancy_meta', $out );
}
endif;

if ( !function_exists( 'mysite_portfolio_date' ) ) :
/**
 *
 */
function mysite_portfolio_date( $date, $args ) {
	extract( $args );
	
	if( !empty( $column ) )
		 return;
		
	elseif( !empty( $thumb ) && $thumb == 'large' )
		return;
		
	else
		return $date;
}
endif;

if ( !function_exists( 'mysite_before_post_sc' ) ) :
/**
 *
 */
function mysite_before_post_sc( $filter_args ) {
	$out = '';
	
	if( strpos( $filter_args['disable'], 'image' ) === false )
		$out .= mysite_get_post_image( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_before_entry_sc' ) ) :
/**
 *
 */
function mysite_before_entry_sc( $filter_args ) {
	$out = '';
	
	extract( $filter_args );
	
	if( strpos( $disable, 'title' ) === false )
		$out .= mysite_post_title( $filter_args );
	
	if( strpos( $disable, 'meta' ) === false )
		$out .= mysite_comment_bubble( $filter_args );
	
	if( ( strpos( $disable, 'meta' ) === false ) && ( $column != 1 && $column != 2 && $thumb != 'large' ) )
		$out .= mysite_post_meta( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_excerpt_more' ) ) :
/**
 *
 */
function mysite_excerpt_more( $more ) {
	return ' [...]';
}
endif;

if ( !function_exists( 'mysite_read_more' ) ) :
/**
 *
 */
function mysite_read_more( $args = array() ) {
	global $post;
	$out = '<a class="post_more_link" href="' . get_permalink( $post->ID ) . '">' . __( 'Read More&nbsp;&rarr;', 'backstop-themes' ) . '</a>&nbsp;&nbsp;';
	return $out;
}
endif;

if ( !function_exists( 'mysite_portfolio_read_more' ) ) :
/**
 *
 */
function mysite_portfolio_read_more( $read_more, $url ) {
	return '<a href="' . $url  . '" class="post_more_link">' . __( 'Read More&nbsp;&rarr;', 'backstop-themes' ) . '</a>&nbsp;&nbsp;';
}
endif;

if ( !function_exists( 'mysite_portfolio_visit_site' ) ) :
/**
 *
 */
function mysite_portfolio_visit_site( $visit_site, $url ) {
	return '<a href="' . $url . '" class="post_more_link">' . __( 'Visit Site&nbsp;&rarr;', 'backstop-themes' ) . '</a>&nbsp;&nbsp;';
}
endif;

if ( !function_exists( 'mysite_fancy_search' ) ) :
/**
 * 
 */
function mysite_fancy_search() {
	
	$out = '<div id="fancy_search" class="search_hidden">';
	$out .= '<form id = "searchform" method="get" action="' . get_bloginfo( 'url' ) . '/">';
	$out .= '<input class="text" name="s" id="s" style="width:0px;margin:0;display:none;" />';
	$out .= '<input type="submit" value="submit" class="submit" />';
	if ( get_query_var( 'lang' ) ) { echo '<input type = "hidden" name = "lang" value = "'.get_query_var('lang').'" />'; }
	$out .= '</form>';
	$out .= '</div>';
	
	echo apply_atomic( 'fancy_search', $out );
}
endif;

if ( !function_exists( 'mysite_comment_bubble' ) ) :
/**
 *
 */
function mysite_comment_bubble( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$number = get_comments_number();
	if( ( $number<1 && !comments_open() ) || ( is_attachment() ) || ( is_singular( 'portfolio' ) ) ) return;
	
	$args = wp_parse_args( $args, $defaults );
	extract( $args );
	
	if ( is_page() && !$shortcode ) return;
	
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	if( is_array( $meta_options ) && in_array( 'comments_meta', $meta_options ) )
		return;
	
	$out = '<div class="post_comments_bubble">[post_comments text="" more="%1$s" one="1" zero="0"]</div>';
	
	if( $echo )
		echo apply_atomic_shortcode( 'comment_bubble', $out );
	else
		return apply_atomic_shortcode( 'comment_bubble', $out );
}
endif;

if ( !function_exists( 'mysite_fancy_search_script' ) ) :
/**
 *
 */
function mysite_fancy_search_script() {
	
	$out = "<script type=\"text/javascript\">
	/* <![CDATA[ */
		jQuery(document).ready(function() {
			var search = jQuery('#fancy_search'),
			    searchField = search.find('input.text');

			var hoverConfigSearch = {
				interval:100,
				timeout:2000,
				over:function(){
					jQuery(searchField).css('display', 'block');
					searchField.animate({width:160}, 400, 'easeInOutQuad').focus();
				},
				out:function(){
					searchField.animate({width:0}, 400, 'easeInOutQuad', function(){ jQuery(searchField).css('display', 'none') } ).blur();
				}
			};
			search.hoverIntent(hoverConfigSearch);
	});
	/* ]]> */
	</script>";
	
	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out );
}
endif;

if ( !function_exists( 'mysite_image_preloading' ) ) :
/**
 *
 */
function mysite_image_preloading() {
	global $mysite;

	if( isset( $mysite->mobile ) )
		return;
	
	$out = "
	<script type=\"text/javascript\">
	/* <![CDATA[ */
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.blog_index_image_load span img', imgAppend: '.blog_index_image_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	
	jQuery( '.one_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.two_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.three_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.four_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	
	jQuery( '.portfolio_gallery.large_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.portfolio_gallery.medium_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load' });
	jQuery( '.portfolio_gallery.small_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load' });
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.portfolio_full_image span img', imgAppend: '.portfolio_full_image' });
	jQuery( '#main_inner' ).preloader({ imgSelector: '.blog_sc_image_load span img', imgAppend: '.blog_sc_image_load' , oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '#main_inner, #sidebar_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load', oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
		}
	});
	jQuery( '#intro_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load', oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
		}
	});
	
	function mysite_jcarousel_setup(c) {
		c.clip.parent().parent().parent().parent().parent().removeClass('noscript');
		var jcarousel_img_load = c.clip.children().children().find('.post_grid_image .portfolio_img_load');
		if( jcarousel_img_load.length>1 ) {
			jcarousel_img_load.each(function(i) {
				var filename = jQuery(this).attr('href'),
					videos=['swf','youtube','vimeo','mov'];
				for(var v in videos){
				    if(filename.match(videos[v])){
						jQuery(this).css('backgroundImage','url(' +assetsUri+ '/play.png)');
					}else{
						jQuery(this).css('backgroundImage','url(' +assetsUri+ '/zoom.png)');
					}
				}
				jQuery(this).next().fadeIn(200);
			});
		}
	}
	
	/* ]]> */
	</script>";

	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out );
}
endif;

?>