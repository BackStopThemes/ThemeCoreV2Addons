<?php
/**
 * The BackStopThemes class. Defines the necessary constants
 * and includes the necessary files for theme's operation.
 *
 * @package BackStopThemesCommon
 */

/**
 * Undocumented class.
 */
class BackStopThemesCommon {
	/**
	 * Initializes the theme framework by loading
	 * required files and functions for the theme.
	 *
	 * @since 1.0
	 *
	 * @param [type] $options - TBD.
	 *
	 * @return void
	 */
	public function init( $options ) {
		$this->load_theme_constants( $options );
		$this->load_theme_specific_constants( $options );
		$this->load_theme_functions();
		$this->load_theme_extensions();
		$this->load_theme_classes();
		$this->load_theme_variables();
		$this->load_theme_actions();
		$this->load_theme_filters();
		$this->load_theme_supports();
		$this->load_theme_locale();
		$this->load_theme_admin();
	}

	/**
	 * Define theme constants.
	 *
	 * @since 1.0
	 *
	 * @param [type] $options - TBD.
	 *
	 * @return void
	 */
	private function load_theme_constants( $options ) {
		define( 'THEME_NAME', $options['theme_name'] );
		define( 'THEME_SLUG', $options['theme_name'] );
		define( 'THEME_VERSION', $options['theme_version'] );
		define( 'FRAMEWORK_VERSION', '3.0' );
		define( 'DOCUMENTATION_URL', '//BackStopThemes.com/docs/index.php/Main_Page' );
		define( 'SUPPORT_URL', '//BackStopThemes.com/support' );
		define( 'MYSITE_PREFIX', 'mysite' );
		define( 'MYSITE_SETTINGS', 'mysite_' . THEME_SLUG . '_options' );
		define( 'MYSITE_INTERNAL_SETTINGS', 'mysite_' . THEME_SLUG . '_internal_options' );
		define( 'MYSITE_SIDEBARS', 'mysite_' . THEME_SLUG . '_sidebars' );
		define( 'MYSITE_SKINS', 'mysite_' . THEME_SLUG . '_skins' );
		define( 'MYSITE_ACTIVE_SKIN', 'mysite_' . THEME_SLUG . '_active_skin' );
		define( 'MYSITE_SKIN_NT_WRITABLE', 'mysite_' . THEME_SLUG . '_skins_nt_writable' );
		define( 'THEME_URI', rtrim( plugin_dir_url( __FILE__ ), '/' ) );
		define( 'THEME_DIR', rtrim( plugin_dir_path( __FILE__ ), '/' ) );
		define( 'THEME_LIBRARY', THEME_DIR . '/lib' );
		define( 'THEME_ADMIN', THEME_LIBRARY . '/admin' );
		define( 'THEME_FUNCTIONS', THEME_LIBRARY . '/functions' );
		define( 'THEME_CLASSES', THEME_LIBRARY . '/classes' );
		define( 'THEME_EXTENSIONS', THEME_LIBRARY . '/extensions' );
		define( 'THEME_SHORTCODES', THEME_LIBRARY . '/shortcodes' );
		define( 'THEME_FONTS', THEME_LIBRARY . '/scripts/fonts' );
		define( 'THEME_STYLES_DIR', THEME_DIR . '/styles' );
		define( 'THEME_PATTERNS_DIR', THEME_STYLES_DIR . '/_patterns' );
		define( 'THEME_SPRITES_DIR', THEME_STYLES_DIR . '/_sprites' );
		define( 'THEME_IMAGES_DIR', THEME_DIR . '/images' );
		define( 'THEME_PATTERNS', '_patterns' );
		define( 'THEME_IMAGES', THEME_URI . '/images' );
		define( 'THEME_IMAGES_ASSETS', THEME_IMAGES . '/assets' );
		define( 'THEME_JS', THEME_URI . '/lib/scripts' );
		define( 'THEME_STYLES', THEME_URI . '/styles' );
		define( 'THEME_SPRITES', THEME_STYLES . '/_sprites' );
		define( 'THEME_ADMIN_FUNCTIONS', THEME_ADMIN . '/functions' );
		define( 'THEME_ADMIN_CLASSES', THEME_ADMIN . '/classes' );
		define( 'THEME_ADMIN_OPTIONS', THEME_ADMIN . '/options' );
		define( 'THEME_ADMIN_ASSETS_URI', THEME_URI . '/lib/admin/assets' );
		define( 'DEFAULT_SKIN', 'default.css' );
	}

	/**
	 * Loads theme functions.
	 *
	 * @since 1.0
	 */
	private function load_theme_functions() {
		require_once THEME_DIR . '/activation.php';
		require_once THEME_FUNCTIONS . '/hooks-actions.php';
		require_once THEME_FUNCTIONS . '/context.php';
		require_once THEME_FUNCTIONS . '/core.php';
		require_once THEME_FUNCTIONS . '/theme.php';
		require_once THEME_FUNCTIONS . '/sliders.php';
		require_once THEME_FUNCTIONS . '/scripts.php';
		require_once THEME_FUNCTIONS . '/image.php';
		require_once THEME_FUNCTIONS . '/bookmarks.php';
		require_once THEME_FUNCTIONS . '/hooks-actions.php';
		require_once THEME_FUNCTIONS . '/compatibility.php';
	}

	/**
	 * Loads theme extensions.
	 *
	 * @since 1.0
	 */
	private function load_theme_extensions() {
		require_once THEME_EXTENSIONS . '/breadcrumbs-plus/breadcrumbs-plus.php';
	}

	/**
	 * Loads theme classes.
	 *
	 * @since 1.0
	 */
	private function load_theme_classes() {
		require_once THEME_CLASSES . '/twitter-api.php';
		require_once THEME_CLASSES . '/contact.php';
		require_once THEME_CLASSES . '/menu-walker.php';
		require_once THEME_CLASSES . '/raw-shortcode.php'; // This filter compensates for WordPress flaw in processing shortcode reurned data.
	}


	/**
	 * Loads theme supports.
	 *
	 * @since 1.0
	 */
	private function load_theme_supports() {
		add_theme_support( 'load_theme_admin_menus' );
		add_theme_support( 'widgets' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
	}

	/**
	 * Handles the locale functions file and translations.
	 *
	 * @since 1.0
	 */
	private function load_theme_locale() {
		// Get the user's locale.
		$locale = get_locale();

		if ( is_admin() ) {
			// Load admin theme textdomain.
			load_theme_textdomain( 'backstop-themes-admin', THEME_ADMIN . '/languages' );
			$locale_file = THEME_ADMIN . "/languages/$locale.php";

		} else {
			// Load theme textdomain.
			load_theme_textdomain( 'backstop-themes', THEME_DIR . '/languages' );
			$locale_file = THEME_DIR . "/languages/$locale.php";
		}

		if ( is_readable( $locale_file ) ) {
			require_once $locale_file;
		}
	}

	/**
	 * Loads admin files.
	 *
	 * @since 1.0
	 */
	private function load_theme_admin() {
		if ( ! is_admin() ) {
			return;
		}

		require_once THEME_ADMIN . '/admin.php';
		( new mysiteAdmin() )->init();
	}
} // close class BackStopThemesCommon.
