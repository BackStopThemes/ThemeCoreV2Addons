<?php
/**
 *
 */
function mysite_options_init() {
	register_setting( MYSITE_SETTINGS, MYSITE_SETTINGS );

	// Add default options if they don't exist
	add_option( MYSITE_SETTINGS, mysite_default_options( 'settings' ) );
	add_option( MYSITE_INTERNAL_SETTINGS, mysite_default_options( 'internal' ) );
	// delete_option(MYSITE_SETTINGS);
	// delete_option(MYSITE_INTERNAL_SETTINGS);
	if ( mysite_ajax_request() ) {
		// Ajax option save
		if ( isset( $_POST['mysite_option_save'] ) ) {
			mysite_ajax_option_save();

			// Sidebar option save
		} elseif ( isset( $_POST['mysite_sidebar_save'] ) ) {
			mysite_sidebar_option_save();

		} elseif ( isset( $_POST['mysite_sidebar_delete'] ) ) {
			mysite_sidebar_option_delete();

		} elseif ( isset( $_POST['action'] ) && $_POST['action'] == 'add-menu-item' ) {
			add_filter(
				'nav_menu_description',
				function() {
					return '';
				}
			);
		}
	}

	// Option import
	if ( ( ! mysite_ajax_request() ) && ( isset( $_POST['mysite_import_options'] ) ) ) {
		mysite_import_options( $_POST[ MYSITE_SETTINGS ]['import_options'] );

		// Reset options
	} elseif ( ( ! mysite_ajax_request() ) && ( isset( $_POST[ MYSITE_SETTINGS ]['reset'] ) ) ) {
		update_option( MYSITE_SETTINGS, mysite_default_options( 'settings' ) );
		delete_option( MYSITE_SIDEBARS );
		wp_redirect( admin_url( 'admin.php?page=mysite-options&reset=true' ) );
		exit;

		// $_POST option save
	} elseif ( ( ! mysite_ajax_request() ) && ( isset( $_POST['mysite_admin_wpnonce'] ) ) ) {
		unset( $_POST[ MYSITE_SETTINGS ]['export_options'] );
	}

}

/**
 *
 */
function mysite_sidebar_option_delete() {
	check_ajax_referer( MYSITE_SETTINGS . '_wpnonce', 'mysite_admin_wpnonce' );

	$data = $_POST;

	$saved_sidebars = get_option( MYSITE_SIDEBARS );

	$msg = array(
		'success'    => false,
		'sidebar_id' => $data['sidebar_id'],
		'message'    => sprintf( __( 'Error: Sidebar &quot;%1$s&quot; not deleted, please try again.', 'backstop-themes-admin' ), $data['mysite_sidebar_delete'] ),
	);

	unset( $saved_sidebars[ $data['sidebar_id'] ] );

	if ( update_option( MYSITE_SIDEBARS, $saved_sidebars ) ) {
		$msg = array(
			'success'    => 'deleted_sidebar',
			'sidebar_id' => $data['sidebar_id'],
			'message'    => sprintf( __( 'Sidebar &quot;%1$s&quot; Deleted.', 'backstop-themes-admin' ), $data['mysite_sidebar_delete'] ),
		);
	}

	$echo = json_encode( $msg );

	@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
	echo $echo;
	exit;
}

/**
 *
 */
function mysite_sidebar_option_save() {
	check_ajax_referer( MYSITE_SETTINGS . '_wpnonce', 'mysite_admin_wpnonce' );

	$data = $_POST;

	$saved_sidebars = get_option( MYSITE_SIDEBARS );

	$msg = array(
		'success' => false,
		'sidebar' => $data['custom_sidebars'],
		'message' => sprintf( __( 'Error: Sidebar &quot;%1$s&quot; not saved, please try again.', 'backstop-themes-admin' ), $data['custom_sidebars'] ),
	);

	if ( empty( $saved_sidebars ) ) {
		$update_sidebar[ $data['mysite_sidebar_id'] ] = $data['custom_sidebars'];

		if ( update_option( MYSITE_SIDEBARS, $update_sidebar ) ) {
			$msg = array(
				'success'    => 'saved_sidebar',
				'sidebar'    => $data['custom_sidebars'],
				'sidebar_id' => $data['mysite_sidebar_id'],
				'message'    => sprintf( __( 'Sidebar &quot;%1$s&quot; Added.', 'backstop-themes-admin' ), $data['custom_sidebars'] ),
			);
		}
	} elseif ( is_array( $saved_sidebars ) ) {

		if ( in_array( $data['custom_sidebars'], $saved_sidebars ) ) {
			$msg = array(
				'success' => false,
				'sidebar' => $data['custom_sidebars'],
				'message' => sprintf( __( 'Sidebar &quot;%1$s&quot; Already Exists.', 'backstop-themes-admin' ), $data['custom_sidebars'] ),
			);

		} elseif ( ! in_array( $data['custom_sidebars'], $saved_sidebars ) ) {
			$sidebar[ $data['mysite_sidebar_id'] ] = $data['custom_sidebars'];
			$update_sidebar                        = $saved_sidebars + $sidebar;

			if ( update_option( MYSITE_SIDEBARS, $update_sidebar ) ) {
				$msg = array(
					'success'    => 'saved_sidebar',
					'sidebar'    => $data['custom_sidebars'],
					'sidebar_id' => $data['mysite_sidebar_id'],
					'message'    => sprintf( __( 'Sidebar &quot;%1$s&quot; Added.', 'backstop-themes-admin' ), $data['custom_sidebars'] ),
				);
			}
		}
	}

	$echo = json_encode( $msg );

	@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
	echo $echo;
	exit;
}

/**
 *
 */
function mysite_ajax_option_save() {
	check_ajax_referer( MYSITE_SETTINGS . '_wpnonce', 'mysite_admin_wpnonce' );

	$data = $_POST;

	unset( $data['_wp_http_referer'], $data['_wpnonce'], $data['action'], $data['mysite_full_submit'], $data[ MYSITE_SETTINGS ]['export_options'] );
	unset( $data['mysite_admin_wpnonce'], $data['mysite_option_save'], $data['option_page'] );

	// TinyMCE editor IDs cannot have brackets.
	// This is the fix for now.
	foreach ( (array) $data as $key => $value ) {
		if ( strpos( $key, '-bracket-' ) !== false ) {
			$option_name                                = explode( '-bracket-', $key );
			$data[ MYSITE_SETTINGS ][ $option_name[1] ] = $value;
			unset( $data[ $key ] );
		}
	}

	$msg = array(
		'success' => false,
		'message' => __( 'Error: Options not saved, please try again.', 'backstop-themes-admin' ),
	);

	$twitter_verify_api = BackStopThemes_Twitter::get_instance()->verify_api( $data[ MYSITE_SETTINGS ] );

	if ( get_option( MYSITE_SETTINGS ) != $data[ MYSITE_SETTINGS ] ) {

		if ( update_option( MYSITE_SETTINGS, $data[ MYSITE_SETTINGS ] ) ) {

			if ( $twitter_verify_api ) {
				$msg = array(
					'success' => 'options_saved',
					'message' => __( 'Options Saved.', 'backstop-themes-admin' ),
				);
			} else {
				$msg = array(
					'success'     => 'options_saved',
					'message'     => __( 'Options Saved, but your Twitter API settings are invalid, please verify these settings are correct.', 'backstop-themes-admin' ),
					'image_error' => true,
				);
			}
		}
	} else {

		if ( $twitter_verify_api ) {
			$msg = array(
				'success' => true,
				'message' => __( 'Options Saved.', 'backstop-themes-admin' ),
			);
		} else {
			$msg = array(
				'success'     => true,
				'message'     => __( 'Options Saved, but your Twitter API settings are invalid, please verify these settings are correct.', 'backstop-themes-admin' ),
				'image_error' => true,
			);
		}
	}

	$echo = json_encode( $msg );

	@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
	echo $echo;
	exit;
}

/**
 * Undocumented function
 *
 * This function provides an end-user admin interface for adding shortcodes to a page / post in the editor.
 *
 * NOTE: It is assumed that all shortcode files in the shortcodes directory has a public scg_options function
 * that can be called to provide the shortcode generator data for the shortcode. This approach allows all
 * shortcodes to be self-contained in a single shortcode-specific PHP file.
 *
 * @return array
 */
function mysite_shortcode_generator() {

	$shortcode_files = mysite_shortcode_files();
	$sc_list         = [];

	foreach ( (array) $shortcode_files as $shortcode_file ) {
		$classname = preg_replace( '/[0-9-_]/', '', str_replace( '.php', '', $shortcode_file ) );
		$sc_class  = 'mysite' . ucfirst( $classname );
		if ( 'hidden' !== $classname ) { // Do not make hidden internal-use shortcodes visible to the end-user.
			$class_object  = new $sc_class();
			$class_methods = get_class_methods( $sc_class );
			$shortcode     = array();

			foreach ( (array) $class_methods as $method ) {
				if ( '_' !== $method[0] ) {
					$shortcode[] = call_user_func( array( $class_object, $method ), $atts = 'generator' );
				}
			}
			$sc_info            = [];
			$sc_info['options'] = $shortcode;
			$sc_info['value']   = $classname;
			$sc_info['name']    = __( ucfirst( $classname ), 'backstop-themes-admin' );
			$sc_info['desc']    = __( 'Select the ' . ucfirst( $classname ) . ' type.', 'backstop-themes-admin' );
			$sc_list[]          = $sc_info;
		}
	}
	return $sc_list;
}

/**
 *
 */
function mysite_check_wp_version() {
	global $wp_version;

	$check_WP = '3.0';
	$is_ok    = version_compare( $wp_version, $check_WP, '>=' );

	if ( ( $is_ok == false ) ) {
		return false;
	}

	return true;
}

/**
 *
 */
function mysite_wpmu_style_option() {
	$styles = array();
	if ( is_multisite() ) {
		global $blog_id;
		$wpmu_styles_path = mysite_upload_dir() . '/styles/';
		if ( is_dir( $wpmu_styles_path ) ) {
			if ( $open_dirs = opendir( $wpmu_styles_path ) ) {
				while ( ( $style = readdir( $open_dirs ) ) !== false ) {
					if ( stristr( $style, '.css' ) !== false ) {
						$theme_name          = md5( THEME_NAME ) . 'muskin_';
						$style_mu            = str_replace( $theme_name, '', $style );
						$styles[ $style_mu ] = @filemtime( $wpmu_styles_path . $style );

						if ( stristr( $style, 'muskin_' ) !== false && stristr( $style, $theme_name ) === false ) {
							unset( $styles[ $style_mu ] );
						}
					}
				}
			}
		}
	}

	return $styles;
}

/**
 *
 */
function mysite_style_option() {
	$styles      = array();
	$sort_styles = array();

	if ( is_dir( THEME_STYLES_DIR . '/' ) ) {
		if ( $open_dirs = opendir( THEME_STYLES_DIR . '/' ) ) {
			while ( ( $style = readdir( $open_dirs ) ) !== false ) {
				if ( stristr( $style, '.css' ) !== false ) {
					$styles[ $style ] = @filemtime( THEME_STYLES_DIR . '/' . $style );
				}
			}
		}
	}

	$styles = array_merge( $styles, mysite_wpmu_style_option() );

	arsort( $styles );

	$nt_writable = get_option( MYSITE_SKIN_NT_WRITABLE );
	if ( ! empty( $nt_writable ) ) {
		foreach ( (array) $nt_writable as $key => $val ) {
			$val                 = $val . '.css';
			$sort_styles[ $val ] = $val;
		}
	}

	foreach ( (array) $styles as $key => $val ) {
		$sort_styles[ $key ] = $key;
	}

	unset( $sort_styles['_create_new.css'] );

	return $sort_styles;
}

/**
 *
 */
function mysite_sociable_option() {
	$sociables = array();
	$styles    = array();

	$pic_types = array( 'jpg', 'jpeg', 'gif', 'png' );

	if ( is_dir( THEME_DIR . '/images/sociables/default/' ) ) {
		if ( $open_dirs = opendir( THEME_DIR . '/images/sociables/default/' ) ) {
			while ( ( $sociable = readdir( $open_dirs ) ) !== false ) {
				$parts = explode( '.', $sociable );
				$ext   = strtolower( $parts[ count( $parts ) - 1 ] );

				if ( in_array( $ext, $pic_types ) ) {
					$option                 = str_replace( '_', ' ', $parts[ count( $parts ) - 2 ] );
					$option                 = ucwords( $option );
					$sociables[ $sociable ] = str_replace( ' ', '', $option );
				}
			}
		}
	}

	if ( is_dir( THEME_DIR . '/images/sociables/' ) ) {
		if ( $open_dirs = opendir( THEME_DIR . '/images/sociables/' ) ) {
			while ( ( $style = readdir( $open_dirs ) ) !== false ) {

				$styles[ $style ] = ucwords( str_replace( '_', ' ', $style ) );

				while ( ( $ix = array_search( '.', $styles ) ) > -1 ) {
					unset( $styles[ $ix ] );
				}
				while ( ( $ix = array_search( '..', $styles ) ) > -1 ) {
					unset( $styles[ $ix ] );
				}
			}
		}
	}

	return array(
		'styles'    => $styles,
		'sociables' => $sociables,
	);
}

/**
 *
 */
function mysite_pattern_presets() {
	$patterns  = array();
	$pic_types = array( 'jpg', 'jpeg', 'gif', 'png' );

	if ( is_dir( THEME_PATTERNS_DIR ) ) {
		if ( $open_dirs = opendir( THEME_PATTERNS_DIR ) ) {
			while ( ( $pattern = readdir( $open_dirs ) ) !== false ) {
				$parts = explode( '.', $pattern );
				$ext   = strtolower( $parts[ count( $parts ) - 1 ] );

				if ( in_array( $ext, $pic_types ) ) {
					$patterns[ $pattern ] = $parts[ count( $parts ) - 2 ];
				}
			}
		}
	}

	asort( $patterns );

	return $patterns;
}

/**
 *
 */
function mysite_cufon_fonts() {
	$cufon = array();
	if ( is_dir( THEME_FONTS ) ) {
		if ( $open_dirs = opendir( THEME_FONTS ) ) {
			while ( ( $font = readdir( $open_dirs ) ) !== false ) {
				if ( stristr( $font, '.js' ) !== false ) {
					$font           = str_replace( '.js', '', $font );
					$cufon[ $font ] = ucfirst( $font );
				}
			}
		}
	}

	asort( $cufon );

	return $cufon;
}

/**
 *
 */
function mysite_typography_options() {
	$font = array(
		'Web'                                              => 'Web',
		'Arial, Helvetica, sans-serif'                     => 'Arial',
		'"Copperplate Light", "Copperplate Gothic Light", serif' => 'Copperplate Light',
		'"Courier New", Courier, monospace'                => 'Courier New',
		'Futura, "Century Gothic", AppleGothic, sans-serif' => 'Futura',
		'Georgia, Times, "Times New Roman", serif'         => 'Georgia',
		'"Gill Sans", Calibri, "Trebuchet MS", sans-serif' => 'Gill Sans',
		'Impact, Haettenschweiler, "Arial Narrow Bold", sans-serif' => 'Impact',
		'"Lucida Sans", "Lucida Grande", "Lucida Sans Unicode", sans-serif' => 'Lucida',
		'Palatino, "Palatino Linotype", Georgia, Times, "Times New Roman", serif' => 'Palatino',
		'Tahoma, Geneva, Verdana, sans-serif'              => 'Tahoma',
		'"Times New Roman", Times, Georgia, serif'         => 'Times New Roman',
		'"Trebuchet MS", Tahoma, Arial, sans-serif'        => 'Trebuchet',
		'Verdana, Geneva, Tahoma, sans-serif'              => 'Verdana',
		'inherit'                                          => 'Inherit',
		'optgroup'                                         => 'optgroup',
	);

	$cufon = mysite_cufon_fonts();

	if ( ! empty( $cufon ) ) {
		array_unshift( $cufon, 'Cufon' );
		array_push( $cufon, 'optgroup' );

		$font = array_merge( $font, $cufon );
	}

	$size   = range( 1, 100 );
	$weight = array( 'normal', 'bold' );
	$style  = array( 'normal', 'italic', 'oblique' );

	$options = array(
		'font-size'   => $size,
		'font-weight' => $weight,
		'font-style'  => $style,
		'font-family' => $font,
	);

	return $options;
}

/**
 *
 */
function mysite_not_save_action( $post_id ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { // Autosave, do nothing
			return true;
	}
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { // AJAX? Not used here
			return true;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ) { // Check user permissions
			return true;
	}

	if ( false !== wp_is_post_revision( $post_id ) ) { // Return if it's a post revision
			return true;
	}
	return false;
}

function check_dependencies( $post, $dcheck_string, $dcheck_type ) {

	switch ( $dcheck_type ) {

		case 'preg_match':
			if ( isset( $post->post_content ) ) {
				if ( preg_match( (string) $dcheck_string, $post->post_content ) !== false ) {
					return true;
				}
			}
			if ( isset( $_POST[ MYSITE_SETTINGS ] ) ) {
				if ( isset( $_POST[ MYSITE_SETTINGS ]['_intro_custom_html'] ) && ( preg_match( $dcheck_string, $_POST[ MYSITE_SETTINGS ]['_intro_custom_html'] ) ) ) {
					return true;
				}
				if ( isset( $_POST[ MYSITE_SETTINGS ]['_intro_custom_text'] ) && ( preg_match( $dcheck_string, $_POST[ MYSITE_SETTINGS ]['_intro_custom_text'] ) ) ) {
					return true;
				}
			}
			break;

		case 'strpos':
			if ( isset( $post->post_content ) ) {
				if ( strpos( $post->post_content, (string) $dcheck_string ) !== false ) {
					return true;
				}
			}
			if ( isset( $_POST[ MYSITE_SETTINGS ] ) ) {
				if ( isset( $_POST[ MYSITE_SETTINGS ]['_intro_custom_html'] ) && ( strpos( $_POST[ MYSITE_SETTINGS ]['_intro_custom_html'], $dcheck_string ) ) ) {
					return true;
				}
				if ( isset( $_POST[ MYSITE_SETTINGS ]['_intro_custom_text'] ) && ( strpos( $_POST[ MYSITE_SETTINGS ]['_intro_custom_text'], $dcheck_string ) ) ) {
					return true;
				}
			}
			break;
	}
	return false;
}

/**
 * -
 */
function mysite_dependencies( $post_id, $post, $update ) {
	if ( ! is_admin() || empty( $post_id ) || mysite_not_save_action( $post_id ) ) {
		return;
	}

	$dependencies = array();

	if ( check_dependencies( $post, '/\[portfolio_grid (.*)fancy_layout(\s)?=(\s)?\\\"(\s)?true(\s)?\\\"/', 'preg_match' ) ) {
		$dependencies[] = 'fancy_portfolio';
	};
	if ( check_dependencies( $post, '[nivo', 'strpos' ) ) {
		$dependencies[] = 'nivo';
		echo '<br>NIVO DEPCHECK OK<br>';
	};
	if ( check_dependencies( $post, '[galleria', 'strpos' ) ) {
		$dependencies[] = 'galleria';
	};
	if ( check_dependencies( $post, '[tab', 'strpos' ) ) {
		$dependencies[] = 'tabs';
	};
	if ( check_dependencies( $post, '[tooltip', 'strpos' ) ) {
		$dependencies[] = 'tooltip';
	};
	if ( check_dependencies( $post, '[jcarousel', 'strpos' ) ) {
		$dependencies[] = 'jcarousel';
	};
	if ( check_dependencies( $post, '[contactform', 'strpos' ) ) {
		$dependencies[] = 'contactform';
	};
	if ( check_dependencies( $post, 'post_content=\"full', 'strpos' ) ) {
		$dependencies[] = 'all_scripts';
	};
	$dependencies = serialize( $dependencies );
	update_post_meta( $post_id, '_dependencies', $dependencies );
}

/**
 *
 */
function mysite_tinymce_init_size() {
	global $wp_version;

	if ( isset( $_GET['page'] ) ) {
		if ( $_GET['page'] == 'mysite-options' ) {

			if ( version_compare( $wp_version, '3.3', '>=' ) ) {
				$tinymce = 'TinyMCE_' . MYSITE_SETTINGS . '[content]_size';
			} else {
				$tinymce = 'TinyMCE_' . MYSITE_SETTINGS . '_content_size';
			}

			if ( ! isset( $_COOKIE[ $tinymce ] ) ) {
				setcookie( $tinymce, 'cw=577&ch=251' );
			}
		}
	}
}

/**
 *
 */
function mysite_import_options( $import ) {

	$imported_options = mysite_decode( $import, $serialize = true );

	if ( is_array( $imported_options ) ) {

		if ( array_key_exists( 'backstopthemes_options_export', $imported_options ) ) {
			if ( get_option( MYSITE_SETTINGS ) != $imported_options ) {

				// Run options filter
				$imported_options = mysite_options_filter( $imported_options );

				// Update our options with imported options
				if ( update_option( MYSITE_SETTINGS, $imported_options ) ) {
					wp_redirect( admin_url( 'admin.php?page=mysite-options&import=true' ) );
				} else {
					wp_redirect( admin_url( 'admin.php?page=mysite-options&import=false' ) );
				}
			} else {
				wp_redirect( admin_url( 'admin.php?page=mysite-options&import=true' ) );
			}
		} else {
			wp_redirect( admin_url( 'admin.php?page=mysite-options&import=false' ) );
		}
	} else {
		wp_redirect( admin_url( 'admin.php?page=mysite-options&import=false' ) );
	}

	exit;
}

/**
 *
 */
function mysite_default_site_widgets() {
	$widgets_options_array = array(
		'Quick Start Links' => "<ul class='activation_link_list'>
				<li><a href='//%site_url%/feature-list'>Feature List</a></li>
				<li><a href='//%site_url%/documentation'>Documentation</a></li>
				<li><a href='//%site_url%/faq'>FAQ</a></li>
				<li><a href='//%site_url%/about'>About Us</a></li>
				</ul>",
		'Features'          => "<ul class='activation_link_list'>
				<li><a href='//%site_url%/documentation/typography/'>Typography</a></li>
				<li><a href='//%site_url%/documentation/shortcodes/'>Feature Shortcodes</a></li>
				<li><a href='//%site_url%/documentation/shortcode/sliders/'>Slider Options</a></li>
				<li><a href='//%site_url%/documentation/shortcode/video/'>Video</a></li>
				<li><a href='//%site_url%/documentation/shortcode/social-media/'>Social Media</a></li>
				<li><a href='//%site_url%/documentation/widgets/'>Widgets</a></li>
				<li><a href='//%site_url%/documentation/seo/'>SEO</a></li>
				</ul>",
		'Layouts'           => "<ul class='activation_link_list'>
				<li><a href='//%site_url%/documentation/layout#full-width/'>Full Width</a></li>
				<li><a href='//%site_url%/documentation/layout#left-sidebar/'>Left Sidebar</a></li>
				<li><a href='//%site_url%/documentation/layout#right-sidebar/'>Right Sidebar</a></li>
				<li><a href='//%site_url%/documentation/layout#column-layouts/'>Column Layouts</a></li>
				</ul>",
		'Portfolios'        => "<ul class='activation_link_list'>
				<li><a href='//%site_url%/documentation/shortcode/portfolio#portfolio-1-column/'>Portfolio 1 Column</a></li>
				<li><a href='//%site_url%/documentation/shortcode/portfolio#portfolio-2-columns/'>Portfolio 2 Columns</a></li>
				<li><a href='//%site_url%/documentation/shortcode/portfolio#portfolio-3-columns/'>Portfolio 3 Columns</a></li>
				<li><a href='//%site_url%/documentation/shortcode/portfolio#portfolio-4-columns/'>Portfolio 4 Columns</a></li>
				</ul>",
		'Theme Tools'       => "<ul class='activation_link_list'>
				<li><a href='//%site_url%/documentation/theme-tool#shortcode-generator/'>Shortcode Generator</a></li>
				<li><a href='//%site_url%/documentation/theme-tool#skin-generator/'>Skin Generator</a></li>
				<li><a href='//%site_url%/documentation/theme-tool#admin-panel/'>Admin Panel</a></li>
				</ul>",
		'Demo Pages'        => "<ul class='activation_link_list'>
				<li><a href='//%theme_name%.%site_url%/demo-page/testimonials/'>Testimonials</a></li>
				<li><a href='//%theme_name%.%site_url%/demo-page/portfolios/'>Portfolios</a></li>
				<li><a href='//%theme_name%.%site_url%/demo-page/about-us/'>About Us</a></li>
				<li><a href='//%theme_name%.%site_url%/demo-page/faqs/'>FAQs</a></li>
				<li><a href='//%theme_name%.%site_url%/demo-page/404-error-page/'>404 Error Page</a></li>
				<li><a href='//%theme_name%.%site_url%/demo-page/contact/'>Contact</a></li>
				<li><a href='//%theme_name%.%site_url%/demo-page/sitemap/'>Sitemap</a></li>
				</ul>",
	);

	return $widgets_options_array;
}

/**
 *
 */
function mysite_default_options( $type ) {
	global $mysite;

	$options = '';

	// Default theme settings
	if ( $type == 'settings' ) {

		// Get initial filtered default options
		$default_options = mysite_options_filter( mysite_default_site_options() );

		// Add default image sizes to options array
		foreach ( (array) ( $mysite->layout['images'] ) as $img_key_full => $image_full ) {
			$image_sizes_full['w']                 = $image_full[0];
			$image_sizes_full['h']                 = $image_full[1];
			$images_full[ "${img_key_full}_full" ] = $image_sizes_full;
		}
		foreach ( (array) ( $mysite->layout['big_sidebar_images'] ) as $img_key_big => $image_big ) {
			$image_sizes_big['w']               = $image_big[0];
			$image_sizes_big['h']               = $image_big[1];
			$images_big[ "${img_key_big}_big" ] = $image_sizes_big;
		}
		foreach ( (array) ( $mysite->layout['small_sidebar_images'] ) as $img_key_small => $image_small ) {
			$image_sizes_small['w']                   = $image_small[0];
			$image_sizes_small['h']                   = $image_small[1];
			$images_small[ "${img_key_small}_small" ] = $image_sizes_small;
		}
		// Merge default options & images sizes
		$image_merge1 = array_merge( $default_options, $images_full );
		$image_merge2 = array_merge( $image_merge1, $images_big );
		$options      = array_merge( $image_merge2, $images_small );

	}

	// Interanl framework settings
	if ( $type == 'internal' ) {
		$options = array();

		if ( defined( 'FRAMEWORK_VERSION' ) ) {
			$options['framework_version'] = FRAMEWORK_VERSION;
		}

		if ( defined( 'DOCUMENTATION_URL' ) ) {
			$options['documentation_url'] = DOCUMENTATION_URL;
		}

		if ( defined( 'SUPPORT_URL' ) ) {
			$options['support_url'] = SUPPORT_URL;
		}
	}

	// Default activation widgets
	if ( $type == 'widgets' ) {

		$widget_text = array();
		$widgets     = mysite_default_site_widgets();
		$i           = 2;
		foreach ( (array) $widgets as $key => $value ) {
			$text              = str_replace( '%theme_name%', strtolower( THEME_NAME ), str_replace( '%site_url%', THEME_IMAGES . '/assets', str_replace( '%site_url_img%', THEME_IMAGES . '/activation', $value ) ) );
			$widget_text[ $i ] = array(
				'title'  => $key,
				'text'   => $text,
				'filter' => array(),
			);
			$i++;
		}

		update_option( 'widget_text', $widget_text + array( '_multiwidget' => (int) count( $widget_text ) ) );

		$widget_contact = array(
			'2' => array(
				'title'          => '',
				'email'          => '',
				'enable_captcha' => 0,
				'enable_akismet' => 0,
			),
		);
		update_option( 'widget_contact_form', $widget_contact + array( '_multiwidget' => 1 ) );

		$widget_search = array( '2' => array( 'title' => '' ) );
		update_option( 'widget_search', $widget_search + array( '_multiwidget' => 1 ) );

		$widget_flickr = array(
			'2' => array(
				'title'   => '',
				'id'      => '63171478@N03',
				'number'  => 8,
				'display' => 'latest',
				'size'    => 's',
			),
		);
		update_option( 'widget_flickr', $widget_flickr + array( '_multiwidget' => 1 ) );

		$sidebars_widgets = array(
			'primary' => array( 'text-2' ),
			'home'    => array(),
			'footer1' => array( 'text-3' ),
			'footer2' => array( 'text-4' ),
			'footer3' => array( 'text-5' ),
			'footer4' => array( 'text-6' ),
			'footer5' => array( 'text-7' ),
			'footer6' => array( 'contact_form-2' ),
		);

		update_option( 'sidebars_widgets', $sidebars_widgets );

		return;
	}

	return $options;
}

/**
 *
 */
function mysite_options_filter( $options ) {

	// Check for %site_url% macro and replace with theme image activation url path
	foreach ( $options as $key => $value ) {
		if ( is_array( $value ) ) {
			foreach ( (array) $value as $key2 => $value2 ) {
				$options[ $key ][ $key2 ] = str_replace( '%site_url%', THEME_IMAGES . '/activation', $value2 );
			}
		}
	}

	if ( isset( $options['content'] ) && ! empty( $options['content'] ) ) {
		$options['content'] = str_replace( '%site_url%', THEME_IMAGES . '/activation', $options['content'] );
	}

	return $options;
}

/**
 *
 */
function delete_mysite_postspage_keywords() {
	delete_transient( 'mysite_postspage_keywords' );
}
