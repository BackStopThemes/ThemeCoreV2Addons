<?php
/**
 * Highlights Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteHighlights {

	/**
	 *
	 */
	public function highlight1( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Highlight 1', 'backstop-themes-admin' ),
				'value'   => 'highlight1',
				'options' => array(
					array(
						'name'    => __( 'Highlight Text', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the text that you wish to display with your highlight.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your highlight.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					array(
						'name' => __( 'Custom BG Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Or you can also choose your own color to use as the background for your highlight.', 'backstop-themes-admin' ),
						'id'   => 'bgColor',
						'type' => 'color',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can change the color of the text that appears on your highlight.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'variation' => '',
				'bgcolor'   => '',
				'textcolor' => '',
			),
			$atts
		);

		$variation = $args['variation'];
		$bgcolor   = $args['bgcolor'];
		$textcolor = $args['textcolor'];

		$variation = ( ( $variation ) && ( empty( $bgcolor ) ) ) ? ' ' . trim( $variation ) : '';

		$styles = array();

		if ( $bgcolor ) {
			$styles[] = 'background-color:' . $bgcolor . ';border-color:' . $bgcolor . ';';
		}

		if ( $textcolor ) {
			$styles[] = 'color:' . $textcolor . ';';
		}

		$style = join( '', array_unique( $styles ) );
		$style = ( ! empty( $style ) ) ? ' style="' . $style . '"' : '';

		return '<span class="highlight' . $variation . '"' . $style . '>' . mysite_remove_wpautop( $content ) . '</span>';
	}

	/**
	 *
	 */
	public function highlight2( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Highlight 2', 'backstop-themes-admin' ),
				'value'   => 'highlight2',
				'options' => array(
					array(
						'name'    => __( 'Highlight Text', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the text that you wish to display with your highlight.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your highlight.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can change the color of the text that appears on your highlight.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'variation' => '',
				'textcolor' => '',
			),
			$atts
		);

		$variation = $args['variation'];
		$textcolor = $args['textcolor'];

		$variation = ( ( $variation ) && ( empty( $textcolor ) ) ) ? ' ' . trim( $variation ) . '_text' : '';

		$style = ( ! empty( $textcolor ) ) ? ' style="color:' . $textcolor . ';"' : '';

		return '<span class="highlight2' . $variation . '"' . $style . '>' . mysite_remove_wpautop( $content ) . '</span>';
	}

	/**
	 *
	 */
	public function highlight3( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Highlight 3', 'backstop-themes-admin' ),
				'value'   => 'highlight3',
				'options' => array(
					array(
						'name'    => __( 'Highlight Text', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the text that you wish to display with your highlight.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = ' style="' . $css . '"';
		}

		if ( ! empty( $classes ) ) {
			$classes = ' ' . $classes;
		}

		return '<span class="highlight3' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</span>';
	}

	/**
	 *
	 */
	public function highlight4( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Highlight 4', 'backstop-themes-admin' ),
				'value'   => 'highlight4',
				'options' => array(
					array(
						'name'    => __( 'Highlight Text', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the text that you wish to display with your highlight.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = ' style="' . $css . '"';
		}

		if ( ! empty( $classes ) ) {
			$classes = ' ' . $classes;
		}

		return '<span class="highlight4' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</span>';
	}

}
