<?php
/**
 * Links Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteLinks {

	/**
	 *
	 */
	public function fancy_link( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Fancy Link', 'backstop-themes-admin' ),
				'value'   => 'fancy_link',
				'options' => array(
					array(
						'name'    => __( 'Link Text', 'backstop-themes-admin' ),
						'desc'    => __( 'This is the text that will display as your link.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Link Url', 'backstop-themes-admin' ),
						'desc'    => __( 'Paste the URL that you wish to use for your link here.', 'backstop-themes-admin' ),
						'id'      => 'link',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your link.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Choose your own color to use with your link.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					array(
						'name'    => __( 'Target <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Setting the target to "Blank" will open your page in a new tab when the reader clicks on the button.', 'backstop-themes-admin' ),
						'id'      => 'target',
						'default' => '',
						'options' => array( 'blank' => __( 'Blank', 'backstop-themes-admin' ) ),
						'type'    => 'select',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'link'      => '#',
				'variation' => '',
				'textcolor' => '',
				'target'    => '',
			),
			$atts
		);

		$link      = $args['link'];
		$variation = $args['variation'];
		$target    = $args['target'];
		$textcolor = $args['textcolor'];

		$link      = trim( $link );
		$variation = ( ( $variation ) && ( empty( $textcolor ) ) ) ? " {$variation}_sprite {$variation}_text" : '';
		$color     = ( $textcolor ) ? ' style="color:' . $textcolor . ';"' : '';
		$target    = ( $target == 'blank' ) ? ' target_blank' : '';
		$arrow     = ' &#x2192;';

		$out = '<a href="' . esc_url( $link ) . '" class="fancy_link' . $target . $variation . '"' . $color . '>' . mysite_remove_wpautop( $content ) . $arrow . '</a>';
		$out = apply_filters(
			'mysite_fancy_link',
			$out,
			array(
				'link'      => $link,
				'target'    => $target,
				'variation' => $variation,
				'color'     => $color,
				'content'   => $content,
				'arrow'     => $arrow,
			)
		);

		return $out;
	}

	/**
	 *
	 */
	public function download_link( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Download Link', 'backstop-themes-admin' ),
				'value'   => 'download_link',
				'options' => array(
					array(
						'name'    => __( 'Link Text', 'backstop-themes-admin' ),
						'desc'    => __( 'This is the text that will display as your link.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Link Url', 'backstop-themes-admin' ),
						'desc'    => __( 'Paste the URL that you wish to use for your link here.', 'backstop-themes-admin' ),
						'id'      => 'link',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your link.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Target <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Setting the target to "Blank" will open your page in a new tab when the reader clicks on the button.', 'backstop-themes-admin' ),
						'id'      => 'target',
						'default' => '',
						'options' => array( 'blank' => __( 'Blank', 'backstop-themes-admin' ) ),
						'type'    => 'select',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'link'      => '#',
				'variation' => '',
				'target'    => '',
			),
			$atts
		);

		$link      = $args['link'];
		$variation = $args['variation'];
		$target    = $args['target'];

		$link      = trim( $link );
		$variation = ( $variation ) ? " {$variation}_sprite {$variation}_text" : '';

		$target = ( $target == 'blank' ) ? ' target_blank' : '';

		$out = '<a href="' . esc_url( $link ) . '" class="download_link' . $variation . $target . '">' . mysite_remove_wpautop( $content ) . '</a>';

		return $out;

	}

	/**
	 *
	 */
	public function email_link( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Email Link', 'backstop-themes-admin' ),
				'value'   => 'email_link',
				'options' => array(
					array(
						'name'    => __( 'Link Text', 'backstop-themes-admin' ),
						'desc'    => __( 'This is the text that will display as your link.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Email', 'backstop-themes-admin' ),
						'desc'    => __( 'Paste the email that you wish to use here.<br /><br />When the reader clicks on this link an email client will open with your email ready.', 'backstop-themes-admin' ),
						'id'      => 'email',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your link.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Target <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Setting the target to "Blank" will open your page in a new tab when the reader clicks on the button.', 'backstop-themes-admin' ),
						'id'      => 'target',
						'default' => '',
						'options' => array( 'blank' => __( 'Blank', 'backstop-themes-admin' ) ),
						'type'    => 'select',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'email'     => '#',
				'variation' => '',
				'target'    => '',
			),
			$atts
		);

		$email     = $args['email'];
		$variation = $args['variation'];
		$target    = $args['target'];

		$email     = trim( $email );
		$variation = ( $variation ) ? " {$variation}_sprite {$variation}_text" : '';

		$is_email = preg_match( '/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*?[a-z]+$/is', $email );

		if ( $is_email ) {
			$nospam = ( $email ) ? ' rel="' . mysite_nospam( $email ) . '"' : '';

			if ( $email == trim( $content ) ) {
				$content = mysite_nospam( $content );
				$class   = ' email_link_replace';
			} else {
				$class = ' email_link_noreplace';
			}

			$out = '<a href="#"' . $nospam . ' class="email_link' . $class . $variation . '">' . mysite_remove_wpautop( $content ) . '</a>';
		} else {
			$out = '<a href="' . $email . '" class="email_link' . $variation . '">' . mysite_remove_wpautop( $content ) . '</a>';
		}

		return $out;
	}

}
