<?php
/**
 * Miscellaneous Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteMiscellaneous {

	/**
	 *
	 */
	public function fancy_amp( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'Fancy Amp', 'backstop-themes-admin' ),
				'value' => 'fancy_amp',
			);

			return $option;
		}

		return '<span class="fancy_amp">&amp;</span>';
	}

	/**
	 *
	 */
	public function divider( $atts = null, $content = null, $code = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'Divider', 'backstop-themes-admin' ),
				'value' => 'divider',
			);

			return $option;
		}

		return '<div class="divider"></div>';
	}

	/**
	 *
	 */
	public function divider_top( $atts = null, $content = null, $code = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'Divider Top', 'backstop-themes-admin' ),
				'value' => 'divider_top',
			);

			return $option;
		}

		return '<div class="divider top"><a href="#">' . __( 'Top', 'backstop-themes' ) . '</a></div>';
	}

	/**
	 *
	 */
	public function clearboth( $atts = null, $content = null, $code = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'Clearboth', 'backstop-themes-admin' ),
				'value' => 'clearboth',
			);

			return $option;
		}

		return '<div class="clearboth"></div>';
	}

	/**
	 *
	 */
	public function div( $atts = null, $content = null, $code = null ) {
		$option = array(
			'name'    => __( 'Div', 'backstop-themes-admin' ),
			'value'   => 'div',
			'options' => array(
				array(
					'name'    => __( 'Class', 'backstop-themes-admin' ),
					'desc'    => __( 'Type in the name of the class you wish to assign to this div.', 'backstop-themes-admin' ),
					'id'      => 'class',
					'default' => '',
					'type'    => 'text',
				),
				array(
					'name'    => __( 'Style', 'backstop-themes-admin' ),
					'desc'    => __( 'You can set a custom style here for your div.', 'backstop-themes-admin' ),
					'id'      => 'style',
					'default' => '',
					'type'    => 'text',
				),
				array(
					'name'    => __( 'Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type in the content that you wish to display inside this div.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
				'shortcode_has_atts' => true,
			),
		);

		if ( 'generator' === $atts ) {
			return $option;
		}

		$args = shortcode_atts(
			array(
				'style' => '',
				'class' => '',
			),
			$atts
		);

		return '<div class="' . $args['class'] . '" style="' . $args['style'] . '">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function span( $atts = null, $content = null, $code = null ) {
		$option = array(
			'name'  => __( 'Span', 'backstop-themes-admin' ),
			'value' => 'span',
		);

		if ( 'generator' === $atts ) {
			return $option;
		}

		$args = shortcode_atts(
			array(
				'style' => '',
				'class' => '',
			),
			$atts
		);

		return '<span class="' . $args['class'] . '" style="' . $args['style'] . '">' . mysite_remove_wpautop( $content ) . '</span>';
	}

	/**
	 *
	 */
	public function teaser( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'Teaser', 'backstop-themes-admin' ),
				'value' => 'teaser',
			);

			return $option;
		}

		return '<p class="teaser"><span>' . mysite_remove_wpautop( $content ) . '</span></p>';
	}

	/**
	 *
	 */
	public function hidden( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'Hidden', 'backstop-themes-admin' ),
				'value' => 'hidden',
			);

			return $option;
		}

		return '<div class="hidden">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin10( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin10', 'backstop-themes-admin' ),
				'value' => 'margin10',
			);

			return $option;
		}

		return '<div class="margin10">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin20( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin20', 'backstop-themes-admin' ),
				'value' => 'margin20',
			);

			return $option;
		}

		return '<div class="margin20">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin30( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin30', 'backstop-themes-admin' ),
				'value' => 'margin30',
			);

			return $option;
		}

		return '<div class="margin30">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin40( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin40', 'backstop-themes-admin' ),
				'value' => 'margin40',
			);

			return $option;
		}

		return '<div class="margin40">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin50( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin50', 'backstop-themes-admin' ),
				'value' => 'margin50',
			);

			return $option;
		}

		return '<div class="margin50">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin60( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin60', 'backstop-themes-admin' ),
				'value' => 'margin60',
			);

			return $option;
		}

		return '<div class="margin60">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin70( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin70', 'backstop-themes-admin' ),
				'value' => 'margin70',
			);

			return $option;
		}

		return '<div class="margin70">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin80( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin80', 'backstop-themes-admin' ),
				'value' => 'margin80',
			);

			return $option;
		}

		return '<div class="margin80">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function margin90( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'  => __( 'margin90', 'backstop-themes-admin' ),
				'value' => 'margin90',
			);

			return $option;
		}

		return '<div class="margin90">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function mobile_only( $atts = null, $content = null ) {

		$option = array(
			'name'    => __( 'Mobile Only', 'backstop-themes-admin' ),
			'value'   => 'mobile_only',
			'options' => array(
				array(
					'name'    => __( 'Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type in the content that you wish to display on a mobile device only.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
				'shortcode_has_atts' => true,
			),
		);

		if ( 'generator' === $atts ) {
			return $option;
		}

		global $mysite;

		if ( ! isset( $mysite->mobile ) ) {
			$content = '';
		}

		return $content;
	}

}
