<?php
/**
 * Charts Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteCharts {

	private static $chart_id = 1;

	/**
	 *
	 */
	public function _chart_id() {
		return self::$chart_id++;
	}

	/**
	 *
	 */
	public function chart( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Charts', 'backstop-themes-admin' ),
				'value'   => 'chart',
				'options' => array(
					array(
						'name'                    => __( 'Type', 'backstop-themes-admin' ),
						'desc'                    => __( 'Select which type of chart you would like to use.', 'backstop-themes-admin' ),
						'id'                      => 'type',
						'options'                 => array(
							'area'        => __( 'Area', 'backstop-themes-admin' ),
							'bar'         => __( 'Bar', 'backstop-themes-admin' ),
							'candlestick' => __( 'Candlestick', 'backstop-themes-admin' ),
							'column'      => __( 'Column', 'backstop-themes-admin' ),
							'combo'       => __( 'Combo', 'backstop-themes-admin' ),
							'line'        => __( 'Line', 'backstop-themes-admin' ),
							'pie'         => __( 'Pie', 'backstop-themes-admin' ),
							'scatter'     => __( 'Scatter', 'backstop-themes-admin' ),
						),
						'type'                    => 'select',
						'shortcode_dont_multiply' => true,
					),
					array(
						'name'    => __( 'Title', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the title of your chart.', 'backstop-themes-admin' ),
						'id'      => 'title',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Width', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the width of your chart.', 'backstop-themes-admin' ),
						'id'      => 'width',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Height', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the height of your chart.', 'backstop-themes-admin' ),
						'id'      => 'height',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Data', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the data of your chart.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					array(
						'name'    => __( 'Options', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out extra options you wish to use.', 'backstop-themes-admin' ),
						'id'      => 'extras',
						'default' => '',
						'type'    => 'textarea',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		global $wp_query, $mysite;

		$args = shortcode_atts(
			array(
				'type'   => 'pie',
				'title'  => '',
				'width'  => '400',
				'height' => '300',
				'extras' => '',
			),
			$atts
		);

		$type   = $args['type'];
		$title  = $args['title'];
		$width  = $args['width'];
		$height = $args['height'];
		$extras = $args['extras'];

		$chart_id = 'chart_id_' . self::_chart_id();

		// Load Google core scripts
		$out  = '<script type="text/javascript" src="https://www.google.com/jsapi"></script>';
		$out .= '<script type="text/javascript">';

		// Load Visualization API and whatever chart libraries are needed
		$out .= 'google.load("visualization", "1.0", {"packages":["corechart"]});';
		$out .= 'google.setOnLoadCallback(drawChart);';

		// Create the data table
		$out .= 'function drawChart() {';
		$out .= $content;

		// Set chart options
		if ( $extras != '' ) {
			$extras = ', ' . $extras; }
		$out .= 'var options = { "title" : "' . $title . '", "width" : ' . $width . ', "height" : ' . $height . ' ' . $extras . ' };';

		// Instantiate and draw our chart, passing in some options.
		$type = ucfirst( $type );
		$out .= 'var chart = new google.visualization.' . $type . 'Chart(document.getElementById("' . $chart_id . '"));';
		$out .= 'chart.draw(data, options);';
		$out .= '}';

		$out .= '</script>';

		$out .= '<div id="' . $chart_id . '"></div>';

		return $out;
	}

}
