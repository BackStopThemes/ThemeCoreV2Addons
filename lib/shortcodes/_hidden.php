<?php
/**
 * Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteHidden {

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_half( $atts = null, $content = null ) {
		return '<div class="one_half">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_half_last( $atts = null, $content = null ) {
		return '<div class="one_half last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_third( $atts = null, $content = null ) {
		return '<div class="one_third">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_third_last( $atts = null, $content = null ) {
		return '<div class="one_third last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function two_third( $atts = null, $content = null ) {
		return '<div class="two_third">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function two_third_last( $atts = null, $content = null ) {
		return '<div class="two_third last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_fourth( $atts = null, $content = null ) {
		return '<div class="one_fourth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_fourth_last( $atts = null, $content = null ) {
		return '<div class="one_fourth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function three_fourth( $atts = null, $content = null ) {
		return '<div class="three_fourth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function three_fourth_last( $atts = null, $content = null ) {
		return '<div class="three_fourth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_fifth( $atts = null, $content = null ) {
		return '<div class="one_fifth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_fifth_last( $atts = null, $content = null ) {
		return '<div class="one_fifth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function two_fifth( $atts = null, $content = null ) {
		return '<div class="two_fifth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function two_fifth_last( $atts = null, $content = null ) {
		return '<div class="two_fifth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function three_fifth( $atts = null, $content = null ) {
		return '<div class="three_fifth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function three_fifth_last( $atts = null, $content = null ) {
		return '<div class="three_fifth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function four_fifth( $atts = null, $content = null ) {
		return '<div class="four_fifth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function four_fifth_last( $atts = null, $content = null ) {
		return '<div class="four_fifth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_sixth( $atts = null, $content = null ) {
		return '<div class="one_sixth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function one_sixth_last( $atts = null, $content = null ) {
		return '<div class="one_sixth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function five_sixth( $atts = null, $content = null ) {
		return '<div class="five_sixth">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function five_sixth_last( $atts = null, $content = null ) {
		return '<div class="five_sixth last">' . mysite_remove_wpautop( $content ) . '</div><div class="clearboth"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function divider_padding( $atts = null, $content = null ) {
		return '<div class="divider_padding"></div>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function fancy_header( $atts = null, $content = null ) {

		$args = shortcode_atts(
			array(
				'variation' => '',
				'bgcolor'   => '',
				'textcolor' => '',
			),
			$atts
		);

		$bgcolor   = $args['bgcolor'];
		$textcolor = $args['textcolor'];
		$variation = $args['variation'];
		$variation = ( ( $variation ) && ( empty( $bgcolor ) ) ) ? ' class="' . trim( $variation ) . '"' : '';

		$styles = array();

		if ( $bgcolor ) {
			$styles[] = 'background-color:' . $bgcolor . ';border-color:' . $bgcolor . ';';
		}
		if ( $textcolor ) {
			$styles[] = 'color:' . $textcolor . ';';
		}
		$style = join( '', array_unique( $styles ) );
		$style = ( ! empty( $style ) ) ? ' style="' . $style . '"' : '';

		return '<h6 class="fancy_header"><span' . $variation . $style . '>' . mysite_remove_wpautop( $content ) . '</span></h6>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function dropcap( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'variation' => '',
			),
			$atts
		);

		$variation = $args['variation'];
		$variation = ( $variation ) ? ' ' . $variation . '_sprite' : '';

		return '<span class="dropcap' . $variation . '">' . mysite_remove_wpautop( $content ) . '</span>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function pullquote( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'quotes'    => '',
				'align'     => '',
				'variation' => '',
				'textcolor' => '',
				'cite'      => '',
				'citelink'  => '',
			),
			$atts
		);

		$quotes    = $args['quotes'];
		$align     = $args['align'];
		$variation = $args['variation'];
		$textcolor = $args['textcolor'];
		$cite      = $args['cite'];
		$citelink  = $args['citelink'];

		$class = array();

		if ( trim( $quotes ) === 'true' ) {
			$class[] = ' quotes';
		}
		if ( preg_match( '/left|right|center/', trim( $align ) ) ) {
			$class[] = ' align' . $align;
		}
		if ( ( $variation ) && ( empty( $textcolor ) ) ) {
			$class[] = ' ' . $variation . '_text';
		}
		$citelink = ( $citelink ) ? ' ,<a href="' . esc_url( $citelink ) . '" class="target_blank">' . $citelink . '</a>' : '';

		$cite = ( $cite ) ? ' <cite>&ndash; ' . $cite . $citelink . '</cite>' : '';

		$style = ( $textcolor ) ? ' style="color:' . $textcolor . ';"' : '';

		$class = join( '', array_unique( $class ) );

		return '<span class="pullquote' . $class . '"' . $style . '>' . mysite_remove_wpautop( $content ) . $cite . '</span>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function highlight( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'variation' => '',
				'bgcolor'   => '',
				'textcolor' => '',
			),
			$atts
		);

		$bgcolor   = $args['bgcolor'];
		$textcolor = $args['textcolor'];
		$variation = $args['variation'];
		$variation = ( ( $variation ) && ( empty( $bgcolor ) ) ) ? ' ' . trim( $variation ) : '';

		$styles = array();

		if ( $bgcolor ) {
			$styles[] = 'background-color:' . $bgcolor . ';border-color:' . $bgcolor . ';';
		}
		if ( $textcolor ) {
			$styles[] = 'color:' . $textcolor . ';';
		}
		$style = join( '', array_unique( $styles ) );
		$style = ( ! empty( $style ) ) ? ' style="' . $style . '"' : '';

		return '<span class="highlight' . $variation . '"' . $style . '>' . mysite_remove_wpautop( $content ) . '</span>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array $atts - User defined attributes in shortcode tag.
	 *
	 * @return string
	 */
	public function post_author( $atts ) {
		$args = shortcode_atts(
			array(
				'before' => '',
				'after'  => '',
				'text'   => __( '<em>Posted by:</em>', 'backstop-themes' ),
			),
			$atts
		);

		return '<span class="meta_author">' . $args['before'] . $args['text'] . ' <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '">' . get_the_author_meta( 'display_name' ) . '</a>' . $args['after'] . '</span>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array $atts - User defined attributes in shortcode tag.
	 *
	 * @return string
	 */
	public function post_date( $atts ) {
		$args = shortcode_atts(
			array(
				'before'       => '',
				'after'        => '',
				'text'         => __( '<em>Posted on: </em>', 'backstop-themes' ),
				'dformat'      => get_option( 'date_format' ),
				'tformat'      => '',
				'dt_separator' => ' @ ',
			),
			$atts
		);

		$current_post_date_time = get_the_date( $args['dformat'] ) . ( empty( $args['tformat'] ) ? '' : ( $args['dt_separator'] . get_the_time( $args['tformat'] ) ) );
		return '<span class="meta_date">' . $args['before'] . $args['text'] . '<a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '" title="' . sprintf( get_the_time( __( 'l, F jS, Y, g:i a', 'backstop-themes' ) ) ) . '">' . sprintf( $current_post_date_time ) . '</a>' . $args['after'] . '</span>';
	}

	/**
	 * Undocumented function
	 *
	 * @param array $atts - User defined attributes in shortcode tag.
	 *
	 * @return string
	 */
	public function post_comments( $atts ) {
		$number = get_comments_number();
		$args   = shortcode_atts(
			array(
				'zero'      => __( '0 Comments', 'backstop-themes' ),
				'one'       => __( '1 Comment', 'backstop-themes' ),
				/* translators: %1$s is replaced with the number of comments */
				'more'      => __( '%1$s Comments', 'backstop-themes' ),
				'css_class' => 'comments-link',
				'none'      => '',
				'text'      => '',
				'before'    => ' ',
				'after'     => '',
			),
			$atts
		);

		if ( 0 === $number && ! comments_open() && ! pings_open() ) {
			if ( $args['none'] ) {
				$comments_link = '<span class="' . esc_attr( $args['css_class'] ) . '">' . $args['none'] . '</span>';
			}
		} elseif ( 0 === $number ) {
			/* translators: %1$s is replaced with the number of comments */
			$comments_link = '<a href="' . get_permalink() . '#respond" title="' . sprintf( __( 'Comment on %1$s', 'backstop-themes' ), the_title_attribute( 'echo=0' ) ) . '">' . $args['zero'] . '</a>';
		} elseif ( 1 === $number ) {
			/* translators: %1$s is replaced with the number of comments */
			$comments_link = '<a href="' . get_comments_link() . '" title="' . sprintf( __( 'Comment on %1$s', 'backstop-themes' ), the_title_attribute( 'echo=0' ) ) . '">' . $args['one'] . '</a>';
		} elseif ( 1 < $number ) {
			/* translators: %1$s is replaced with the number of comments */
			$comments_link = '<a href="' . get_comments_link() . '" title="' . sprintf( __( 'Comment on %1$s', 'backstop-themes' ), the_title_attribute( 'echo=0' ) ) . '">' . sprintf( $args['more'], $number ) . '</a>';
		}

		if ( isset( $comments_link ) ) {
			return '<span class="meta_comments">' . $args['before'] . $args['text'] . $comments_link . $args['after'] . '</span>';
		}
	}

	/**
	 * Undocumented function
	 *
	 * @param array $atts - User defined attributes in shortcode tag.
	 *
	 * @return string
	 */
	public function post_terms( $atts ) {
		global $post;

		$args = shortcode_atts(
			array(
				'id'        => $post->ID,
				'taxonomy'  => 'post_tag',
				'separator' => ', ',
				'before'    => ' ',
				'after'     => '',
				'text'      => __( '<em>Posted in: </em>', 'backstop-themes' ),
			),
			$atts
		);

		$before = '<span class="meta_' . $args['taxonomy'] . '">' . $args['before'] . $args['text'];
		$after  = $args['after'] . '</span>';

		return get_the_term_list( $args['id'], $args['taxonomy'], $before, $args['separator'], $after );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function teaser_small( $atts = null, $content = null ) {
		return '<p class="teaser_small">' . mysite_remove_wpautop( $content ) . '</p>';
	}

	/**
	 * Undocumented function
	 *
	 * @return string
	 */
	public function theme_name() {
		return THEME_NAME;
	}


	/**
	 * Legacy Shortcodes
	 */
	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function frame_left( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'alt'   => '',
				'title' => '',
			),
			$atts
		);

		return do_shortcode( '[image_frame style="framed" align="left" alt="' . $args['alt'] . '" title="' . $args['title'] . '"]' . $content . '[/image_frame]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function frame_right( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'alt'   => '',
				'title' => '',
			),
			$atts
		);

		return do_shortcode( '[image_frame style="framed" align="right" alt="' . $args['alt'] . '" title="' . $args['title'] . '"]' . $content . '[/image_frame]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function frame_center( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'alt'   => '',
				'title' => '',
			),
			$atts
		);

		return do_shortcode( '[image_frame style="framed" align="center" alt="' . $args['alt'] . '" title="' . $args['title'] . '"]' . $content . '[/image_frame]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function simple_box( $atts = null, $content = null ) {
		return do_shortcode( '[colored_box variation="white"]' . $content . '[/colored_box]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function color_box( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'title'     => '',
				'align'     => '',
				'variation' => '',
			),
			$atts
		);

		return do_shortcode( '[titled_box title="' . $args['title'] . '" align="' . $args['align'] . '" variation="' . $args['variation'] . '"]' . $content . '[/titled_box]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function fancy_titled_box( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'title' => '',
			),
			$atts
		);

		return do_shortcode( '[fancy_box title="' . $args['title'] . '"]' . $content . '[/fancy_box]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function arrow_list( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'variation' => '',
			),
			$atts
		);

		return do_shortcode( '[fancy_list variation="' . $args['variation'] . '" type="arrow_list"]' . $content . '[/fancy_list]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function bullet_list( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'variation' => '',
			),
			$atts
		);

		return do_shortcode( '[fancy_list variation="' . $args['variation'] . '" type="bullet_list"]' . $content . '[/fancy_list]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function check_list( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'variation' => '',
			),
			$atts
		);

		return do_shortcode( '[fancy_list variation="' . $args['variation'] . '" type="check_list"]' . $content . '[/fancy_list]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function star_list( $atts = null, $content = null ) {
		$args = shortcode_atts(
			array(
				'variation' => '',
			),
			$atts
		);

		return do_shortcode( '[fancy_list variation="' . $args['variation'] . '" type="star_list"]' . $content . '[/fancy_list]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function pullquote_left( $atts = null, $content = null ) {
		return do_shortcode( '[pullquote align="left"]' . $content . '[/pullquote]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function pullquote_right( $atts = null, $content = null ) {
		return do_shortcode( '[pullquote align="right"]' . $content . '[/pullquote]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function minimal_tabs( $atts = null, $content = null ) {
		$out = '';

		$i = 0;
		foreach ( (array) $atts as $tab ) {
			$tabs[ $i ] = $tab;
			$i++;
		}

		if ( ! preg_match_all( "/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches ) ) {
			return mysite_remove_wpautop( $content );
		} else {
			$counter = count( $matches[0] );
			for ( $i = 0; $i < $counter; $i++ ) {
				$out .= ' [tab title="' . $tabs[ $i ] . '"]' . $matches[5][ $i ] . '[/tab] ';
			}
		}

		return do_shortcode( '[tabs] ' . $out . ' [/tabs]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function framed_tabs( $atts = null, $content = null ) {
		$out = '';

		$i = 0;
		foreach ( (array) $atts as $tab ) {
			$tabs[ $i ] = $tab;
			$i++;
		}

		if ( ! preg_match_all( "/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches ) ) {
			return mysite_remove_wpautop( $content );
		} else {
			$counter = count( $matches[0] );
			for ( $i = 0; $i < $counter; $i++ ) {
				$out .= ' [tab title="' . $tabs[ $i ] . '"]' . $matches[5][ $i ] . '[ /tab ] ';
			}
		}

		return do_shortcode( '[tabs_framed] ' . $out . ' [/tabs_framed]' );
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function raw( $atts = null, $content = null ) {
		return $content;
	}

}
