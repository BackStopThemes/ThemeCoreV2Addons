<?php
/**
 * Icons Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteIcons {

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function icon( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Single Icon', 'backstop-themes-admin' ),
				'value'   => 'icon',
				'options' => array(
					array(
						'name'      => __( 'Select Preset Icon', 'backstop-themes-admin' ),
						'desc'      => __( 'Select one of our icon presets to use.', 'backstop-themes-admin' ),
						'id'        => 'type',
						'default'   => '',
						'shortcode' => 'icon',
						'type'      => 'icon_preset',
					),
					array(
						'id'      => 'style',
						'default' => '',
						'type'    => 'hidden',
					),
					array(
						'name'    => __( 'Upload Custom Icon', 'backstop-themes-admin' ),
						'desc'    => __( 'Upload your own icon to use.', 'backstop-themes-admin' ),
						'id'      => 'custom_icon',
						'default' => '',
						'type'    => 'upload',
					),
					array(
						'name'    => __( 'Alignment', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose an alignment for your icon.', 'backstop-themes-admin' ),
						'id'      => 'align',
						'default' => '',
						'options' => array(
							'left'   => __( 'Left', 'backstop-themes-admin' ),
							'right'  => __( 'Right', 'backstop-themes-admin' ),
							'center' => __( 'Center', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'type'        => '',
				'style'       => '',
				'custom_icon' => '',
				'align'       => '',
			),
			$atts
		);

		$align = $args['align'];

		if ( 'left' === $align ) {
			$align = ' alignleft';
		}
		if ( 'right' === $align ) {
			$align = ' alignright';
		}
		if ( 'center' === $align ) {
			$align = ' aligncenter';
		}

		$src = THEME_IMAGES . '/icons/' . $args['style'] . '/' . $args['type'] . '.png';
		if ( ! empty( $args['custom_icon'] ) ) {
			$src = $args['custom_icon'];
		}

		$out = '<span class = "icon' . $align . '"><img src = "' . $src . '" alt = "" /></span>';

		return $out;
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return string
	 */
	public function icon_teaser( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {

			$option = array(
				'name'    => __( 'Icon Teasers', 'backstop-themes-admin' ),
				'value'   => 'icon_teaser',
				'options' => array(
					array(
						'name'      => __( 'Select Preset Icon', 'backstop-themes-admin' ),
						'desc'      => __( 'Select one of our icon presets to use.', 'backstop-themes-admin' ),
						'id'        => 'type',
						'default'   => '',
						'shortcode' => 'icon_teaser',
						'type'      => 'icon_preset',
					),
					array(
						'id'      => 'style',
						'default' => '',
						'type'    => 'hidden',
					),
					array(
						'name'    => __( 'Upload Custom Icon', 'backstop-themes-admin' ),
						'desc'    => __( 'Upload your own icon to use.', 'backstop-themes-admin' ),
						'id'      => 'custom_icon',
						'default' => '',
						'type'    => 'upload',
					),
					array(
						'name'    => __( 'Title', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out a title to display alongside your icon.', 'backstop-themes-admin' ),
						'id'      => 'title',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Teaser', 'backstop-themes-admin' ),
						'desc'    => __( 'Upload your own icon to use.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					array(
						'name'    => __( 'Link Text', 'backstop-themes-admin' ),
						'desc'    => __( 'If you want a link to display beneath your teaser then type some text here.', 'backstop-themes-admin' ),
						'id'      => 'link_text',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Link Url', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the URL for your link.', 'backstop-themes-admin' ),
						'id'      => 'link',
						'default' => '',
						'type'    => 'text',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'type'        => '',
				'style'       => '',
				'custom_icon' => '',
				'title'       => '',
				'link_text'   => '',
				'link'        => '',
			),
			$atts
		);

		$out = '';

		$src = THEME_IMAGES . '/icons/' . $args['style'] . '/' . $args['type'] . '.png';
		if ( ! empty( $args['custom_icon'] ) ) {
			$src = $args['custom_icon'];
		}

		$out     .= '<div class = "icon_teaser">';
		$out     .= '<span class = "icon"><img src = "' . $src . '" alt = ""></span>';
			$out .= '<div class = "icon_text">';
		if ( ! empty( $args['title'] ) ) {
			$out .= '<h3>' . $args['title'] . '</h3>'; }
		if ( ! empty( $content ) ) {
			$out .= '<p>' . $content . '</p>'; }
		if ( ! empty( $args['link'] ) ) {
			$out .= '<p><a class = "icon_teaser_link" href = "' . $args['link'] . '">' . $args['link_text'] . '</a></p>'; }
			$out .= '</div>';
		$out     .= '</div>';

		return $out;
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $atts - User defined attributes in shortcode tag.
	 * @param string $content - Content to apply shortcode to.
	 *
	 * @return array
	 */
	public function icon_banner( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {

			$option = array(
				'name'    => __( 'Icon Banners', 'backstop-themes-admin' ),
				'value'   => 'icon_banner',
				'options' => array(
					array(
						'name'      => __( 'Select Preset Icon', 'backstop-themes-admin' ),
						'desc'      => __( 'Select one of our icon presets to use.', 'backstop-themes-admin' ),
						'id'        => 'type',
						'default'   => '',
						'shortcode' => 'icon_banner',
						'type'      => 'icon_preset',
					),
					array(
						'id'      => 'style',
						'default' => '',
						'type'    => 'hidden',
					),
					array(
						'name'    => __( 'Upload Custom Icon', 'backstop-themes-admin' ),
						'desc'    => __( 'Upload your own icon to use.', 'backstop-themes-admin' ),
						'id'      => 'custom_icon',
						'default' => '',
						'type'    => 'upload',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'type'        => '',
				'style'       => '',
				'custom_icon' => '',
			),
			$atts
		);

		$src = THEME_IMAGES . '/icons/_banners/' . $args['type'] . '.png';
		if ( ! empty( $args['custom_icon'] ) ) {
			$src = $args['custom_icon'];
		}

		return '<span class = "icon_banner"><span class = "icon"><img src = "' . $src . '" alt = ""></span></span>';
	}

}
