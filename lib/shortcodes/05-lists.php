<?php
/**
 * Lists Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteLists {

	/**
	 *
	 */
	public function fancy_list( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Fancy List', 'backstop-themes-admin' ),
				'value'   => 'fancy_list',
				'options' => array(
					array(
						'name'    => __( 'Style', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose the style of list that you wish to use. Each one has a different icon.', 'backstop-themes-admin' ),
						'id'      => 'style',
						'default' => '',
						'options' => array(
							'arrow_list'     => __( 'Arrow List', 'backstop-themes-admin' ),
							'bullet_list'    => __( 'Bullet List', 'backstop-themes-admin' ),
							'check_list'     => __( 'Check List', 'backstop-themes-admin' ),
							'circle_arrow'   => __( 'Circle Arrow', 'backstop-themes-admin' ),
							'triangle_arrow' => __( 'Triangle Arrow', 'backstop-themes-admin' ),
							'comment_list'   => __( 'Comment List', 'backstop-themes-admin' ),
							'minus_list'     => __( 'Minus List', 'backstop-themes-admin' ),
							'plus_list'      => __( 'Plus List', 'backstop-themes-admin' ),
							'star_list'      => __( 'Star List', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'List Html', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the content of your list.  You need to use the &#60;ul&#62; and &#60;li&#62; elements when typing out your list content.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
						'return'  => true,
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your list.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					'shortcode_has_atts'        => true,
					'shortcode_carriage_return' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'style'     => '',
				'variation' => '',
			),
			$atts
		);

		$style     = $args['style'];
		$variation = $args['variation'];

		$style = ( $style ) ? trim( $style ) : 'arrow_list';

		$variation = ( $variation ) ? ' ' . trim( $variation ) . '_sprite' : '';

		$content = str_replace( '<ul>', '<ul class="fancy_list">', $content );
		$content = str_replace( '<li>', '<li class="' . $style . $variation . '">', $content );

		return mysite_remove_wpautop( $content );
	}

	/**
	 *
	 */
	public function fancy_numbers( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Fancy Numbers', 'backstop-themes-admin' ),
				'value'   => 'fancy_numbers',
				'options' => array(
					array(
						'name'    => __( 'List Html', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the content of your list.  You need to use the &#60;ul&#62; and &#60;li&#62; elements when typing out your list content.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
						'return'  => true,
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your list.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					'shortcode_has_atts'        => true,
					'shortcode_carriage_return' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'       => '',
				'classes'   => '',
				'variation' => '',
			),
			$atts
		);

		$css       = $args['css'];
		$classes   = $args['classes'];
		$variation = $args['variation'];

		$classes = '';

		if ( ! empty( $css ) ) {
			$css = ' style="' . $css . '"';
		}

		if ( ! empty( $classes ) ) {
			$classes .= ' ' . $classes;
		}

		if ( ! empty( $variation ) ) {
			$classes .= ' ' . $variation . '_numbers';
		}

		$content = str_replace( '<ol>', '<ol class="fancy_numbers' . $classes . '"' . $css . '>', $content );

		return mysite_remove_wpautop( do_shortcode( $content ) );
	}

	/**
	 *
	 */
	public function squeeze_list( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Squeeze List', 'backstop-themes-admin' ),
				'value'   => 'squeeze_list',
				'options' => array(
					array(
						'name'    => __( 'Style', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose the style of the squeeze list that you wish to use. Each one has a different icon.', 'backstop-themes-admin' ),
						'id'      => 'style',
						'default' => '',
						'options' => array(
							'checkmark_list' => __( 'Checkmark List', 'backstop-themes-admin' ),
							'arrow_list'     => __( 'Arrow List', 'backstop-themes-admin' ),
							'alert_list'     => __( 'Alert List', 'backstop-themes-admin' ),
							'info_list'      => __( 'Info Arrow', 'backstop-themes-admin' ),
							'no_list'        => __( 'No Arrow', 'backstop-themes-admin' ),
							'plus_list'      => __( 'Plus List', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'List Html', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the content of your list. You need to use the &#60;ul&#62; and &#60;li&#62; elements when typing out your list content.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
						'return'  => true,
					),
					'shortcode_has_atts'        => true,
					'shortcode_carriage_return' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'style'   => '',
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$style   = $args['style'];
		$css     = $args['css'];
		$classes = $args['classes'];

		$style = ( $style ) ? ' ' . trim( $style ) : ' checkmark_list';

		if ( ! empty( $css ) ) {
			$css = ' style="' . $css . '"';
		}

		if ( ! empty( $classes ) ) {
			$classes = ' ' . $classes;
		}

		$content = str_replace( '<ul>', '<ul class="squeeze_list' . $style . $classes . '"' . $css . '>', $content );

		return mysite_remove_wpautop( do_shortcode( $content ) );
	}

}
