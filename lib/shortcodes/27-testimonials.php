<?php
/**
 * Testimonials Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteTestimonials {

	/**
	 * Undocumented function
	 *
	 * @param [type] $atts
	 * @param [type] $content
	 *
	 * @return void
	 */
	public function testimonials_grid( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Testimonials Grid', 'backstop-themes-admin' ),
				'value'   => 'testimonials_grid',
				'options' => array(
					array(
						'name'    => __( 'Number of Testimonials', 'backstop-themes-admin' ),
						'desc'    => __( 'Select the number of testimonials you wish to have displayed on each page.', 'backstop-themes-admin' ),
						'id'      => 'show',
						'options' => array_combine( range( 1, 40 ), array_values( range( 1, 40 ) ) ),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Number of Columns', 'backstop-themes-admin' ),
						'desc'    => __( 'Select the number of columns you would like your testimonials to display in.', 'backstop-themes-admin' ),
						'id'      => 'column',
						'default' => '',
						'options' => array(
							'1' => __( 'One Column', 'backstop-themes-admin' ),
							'2' => __( 'Two Column', 'backstop-themes-admin' ),
							'3' => __( 'Three Column', 'backstop-themes-admin' ),
							'4' => __( 'Four Column', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Style', 'backstop-themes-admin' ),
						'desc'    => __( 'Select the style you would like your testimonials to display in.', 'backstop-themes-admin' ),
						'id'      => 'style',
						'default' => '',
						'options' => array(
							'pullquote1' => __( 'Pullquote 1', 'backstop-themes-admin' ),
							'pullquote2' => __( 'Pullquote 2', 'backstop-themes-admin' ),
							'pullquote3' => __( 'Pullquote 3', 'backstop-themes-admin' ),
							'pullquote4' => __( 'Pullquote 4', 'backstop-themes-admin' ),
							'blockquote' => __( 'Blockquotes', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Testimonials Categories <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'If you want testimonials from specific categories to display then you may choose them here.', 'backstop-themes-admin' ),
						'id'      => 'cat',
						'default' => array(),
						'target'  => 'cat_testimonial',
						'type'    => 'multidropdown',
					),
					array(
						'name'    => __( 'Show Testimonials Pagination <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Checking this will show pagination at the bottom so the reader can go to the next page.', 'backstop-themes-admin' ),
						'id'      => 'pagination',
						'options' => array( 'true' => 'Show Post Pagination' ),
						'type'    => 'checkbox',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'show'       => '4',
				'column'     => '2',
				'style'      => 'blockquote',
				'cat'        => '',
				'pagination' => '',
			),
			$atts
		);

		$show       = $args['show'];
		$column     = $args['column'];
		$style      = $args['style'];
		$cat        = $args['cat'];
		$pagination = $args['pagination'];

		$out = '';

		// Testimonal query options.
		$show       = trim( $show );
		$column     = trim( $column );
		$style      = trim( $style );
		$pagination = trim( $pagination );

		$query = array( 'post_type' => 'testimonial' );

		if ( $pagination == 'true' ) {
			$query['posts_per_page'] = $show;
			$query['paged']          = mysite_get_page_query();

		} else {
			$query['showposts'] = $show;
			$query['nopaging']  = 0;
		}

		if ( ! empty( $cat ) ) {
			$query['tax_query'] = array(
				'relation' => 'IN',
				array(
					'taxonomy' => 'testimonial_category',
					'field'    => 'slug',
					'terms'    => explode( ',', $cat ),
				),
			);
		}

		// Query testimonials.
		$testimonials_query = new WP_Query( $query );

		// Check to see testimonials have be added.
		if ( $testimonials_query->have_posts() ) :
			while ( $testimonials_query->have_posts() ) :
				$testimonials_query->the_post();

				// Loop through and get all testimonial custom fields.
				$post_id       = get_the_ID();
				$custom_fields = get_post_custom( $post_id );
				foreach ( (array) $custom_fields as $key => $value ) {
					$testimonial[ $post_id ][ $key ] = $value[0];
				}

		endwhile; // End testimonials_query loop.

			// Define column classes.
			switch ( $column ) {
				case 1:
					$main_class = 'testimonial_grid one_column_testimonial';
					break;
				case 2:
					$main_class   = 'testimonial_grid two_column_testimonial';
					$column_class = 'one_half';
					break;
				case 3:
					$main_class   = 'testimonial_grid three_column_testimonial';
					$column_class = 'one_third';
					break;
				case 4:
					$main_class   = 'testimonial_grid four_column_testimonial';
					$column_class = 'one_fourth';
					break;
			}

			$out .= '<div class="' . $main_class . '">';

			// Loop through all testimonial custom fields and output testimonials.
			$i = 1;
			foreach ( (array) $testimonial as $key => $value ) {

				$out .= ( $column != 1 ? '<div class="' . ( $i % $column == 0 ? $column_class . ' last' : $column_class ) . '">' : '' );

				if ( ! empty( $value['_testimonial'] ) ) {

					// Pullquote1 style.
					$quote = '[' . $style . ' raw="false" testimonial_sc="true"';
					if ( ! empty( $value['_name'] ) ) {
						$quote .= ' cite_sc="' . $value['_name'] . '"';
					}

					if ( ! empty( $value['_website_url'] ) ) {
						$quote .= ' citelink="' . $value['_website_url'] . '"';
					}

					if ( ! empty( $value['_website_name'] ) ) {
						$quote .= ' citename="' . $value['_website_name'] . '"';
					}

					if ( isset( $value['_image'] ) && $value['_image'] == 'upload_picture' && ! empty( $value['_custom_image'] ) ) {
						$quote .= ' citeimgcustom="' . $value['_custom_image'] . '"';
					}

					if ( isset( $value['_image'] ) && $value['_image'] == 'use_gravatar' ) {
						$value['_email'] = ( isset( $value['_email'] ) ? $value['_email'] : 'sjm6g1LW@sjm6g1LW.com' );
						$quote          .= ' citeimgavatar="' . $value['_email'] . '"';
					}

					$quote .= ']' . $value['_testimonial'];
					$quote .= '[/' . $style . ']';

					$out .= do_shortcode( $quote );
				}

				$out .= ( $column != 1 ? '</div>' : '' );

				if ( ( $i % $column ) == 0 ) {
					$out .= '<div class="clearboth"></div>';
				}

				$i++;
			}

			$out .= '</div>';

			$out .= ( $pagination == 'true' ) ? mysite_pagenavi( '', '', $testimonials_query ) : '';

		else :

			$out .= __( 'No testimonials were found', 'backstop-themes' );

			if ( current_user_can( 'edit_posts' ) ) {
				$out .= '<p><a href="' . esc_url( admin_url( 'post-new.php?post_type=testimonial' ) ) . '">' . __( 'Click here to add testimonials', 'backstop-themes' ) . '</a></p>';
			}

		endif;

		wp_reset_query();

		return $out;
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $atts
	 * @param [type] $content
	 *
	 * @return void
	 */
	public function testimonials_single( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Testimonials Single', 'backstop-themes-admin' ),
				'value'   => 'testimonials_single',
				'options' => array(
					array(
						'name' => __( 'ID', 'backstop-themes-admin' ),
						'desc' => __( 'Enter the ID of testimonial you wish to have displayed.', 'backstop-themes-admin' ),
						'id'   => 'id',
						'type' => 'text',
					),
					array(
						'name'    => __( 'Style', 'backstop-themes-admin' ),
						'desc'    => __( 'Select the style you would like your testimonial to display in.', 'backstop-themes-admin' ),
						'id'      => 'style',
						'default' => '',
						'options' => array(
							'pullquote1' => __( 'Pullquote 1', 'backstop-themes-admin' ),
							'pullquote2' => __( 'Pullquote 2', 'backstop-themes-admin' ),
							'pullquote3' => __( 'Pullquote 3', 'backstop-themes-admin' ),
							'pullquote4' => __( 'Pullquote 4', 'backstop-themes-admin' ),
							'blockquote' => __( 'Blockquotes', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Align <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Set the alignment for your testimonial here.<br /><br />Your testimonial will float along the center, left or right hand sides depending on your choice.', 'backstop-themes-admin' ),
						'id'      => 'align',
						'default' => '',
						'options' => array(
							'left'   => __( 'left', 'backstop-themes-admin' ),
							'right'  => __( 'right', 'backstop-themes-admin' ),
							'center' => __( 'center', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'id'    => '',
				'style' => 'blockquote',
				'align' => '',
			),
			$atts
		);

		$id    = $args['id'];
		$style = $args['style'];
		$align = $args['align'];

		$out = '';

		// Query testimonials.
		$testimonials_query = new WP_Query( "post_type=testimonial&p=$id" );

		if ( ! empty( $id ) && $testimonials_query->have_posts() ) :
			while ( $testimonials_query->have_posts() ) :
				$testimonials_query->the_post();

				// Loop through and get all testimonial custom fields.
				$post_id       = get_the_ID();
				$custom_fields = get_post_custom( $post_id );
				foreach ( (array) $custom_fields as $key => $value ) {
					$testimonial[ $post_id ][ $key ] = $value[0];
				}

		endwhile; // End testimonials_query loop.

			if ( ! empty( $testimonial[ $id ]['_testimonial'] ) ) {
				// Pullquote1 style.
				$quote = '[' . $style . ' raw="false" testimonial_sc="true"';
				if ( ! empty( $align ) ) {
					$quote .= ' align="' . $align . '"';
				}

				if ( ! empty( $testimonial[ $id ]['_name'] ) ) {
					$quote .= ' cite_sc="' . $testimonial[ $id ]['_name'] . '"';
				}

				if ( ! empty( $testimonial[ $id ]['_website_url'] ) ) {
					$quote .= ' citelink="' . $testimonial[ $id ]['_website_url'] . '"';
				}

				if ( ! empty( $testimonial[ $id ]['_website_name'] ) ) {
					$quote .= ' citename="' . $testimonial[ $id ]['_website_name'] . '"';
				}

				if ( isset( $testimonial[ $id ]['_image'] ) && $testimonial[ $id ]['_image'] == 'upload_picture' && ! empty( $testimonial[ $id ]['_custom_image'] ) ) {
					$quote .= ' citeimgcustom="' . $testimonial[ $id ]['_custom_image'] . '"';
				}

				if ( isset( $testimonial[ $id ]['_image'] ) && $testimonial[ $id ]['_image'] == 'use_gravatar' ) {
					$testimonial[ $id ]['_email'] = ( isset( $testimonial[ $id ]['_email'] ) ? $testimonial[ $id ]['_email'] : 'sjm6g1LW@sjm6g1LW.com' );
					$quote                       .= ' citeimgavatar="' . $testimonial[ $id ]['_email'] . '"';
				}

				$quote .= ']' . $testimonial[ $id ]['_testimonial'];
				$quote .= '[/' . $style . ']';

				$out .= do_shortcode( $quote );
			}

			$out = '<div class="testimonial_single">' . $out . '</div>';

		else :

			$out .= __( 'No testimonials were found', 'backstop-themes' );

			if ( current_user_can( 'edit_posts' ) ) {
				$out .= '<p><a href="' . esc_url( admin_url( 'post-new.php?post_type=testimonial' ) ) . '">' . __( 'Click here to add testimonials', 'backstop-themes' ) . '</a></p>';
			}

		endif;

		wp_reset_query();

		return $out;
	}

}
