<?php
/**
 * Tooltip Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteTooltip {

	private static $tip_id = 1;

	/**
	 *
	 */
	public function _tip_id() {
		return self::$tip_id++;
	}

	public function tooltip( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Tooltip', 'backstop-themes-admin' ),
				'value'   => 'tooltip',
				'options' => array(
					array(
						'name' => __( 'Tooltip Trigger Text', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the trigger text that will appear in the post /page.<br /><br />The tooltip will be displayed when hovering over the trigger text.', 'backstop-themes-admin' ),
						'id'   => 'trigger',
						'type' => 'text',
					),
					array(
						'name' => __( 'Tooltip Content', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the content that you wish to have displayed inside the tooltip.', 'backstop-themes-admin' ),
						'id'   => 'content',
						'type' => 'textarea',
					),
					array(
						'name' => __( 'Width <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can manually specify the width of your tooltip here.', 'backstop-themes-admin' ),
						'id'   => 'width',
						'type' => 'text',
					),
					array(
						'name'    => __( 'Tooltip Position <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Select where you would like your tooltip to appear relative to your trigger text.', 'backstop-themes-admin' ),
						'id'      => 'position',
						'options' => array(
							'top'    => __( 'Top', 'backstop-themes-admin' ),
							'right'  => __( 'Right', 'backstop-themes-admin' ),
							'bottom' => __( 'Bottom', 'backstop-themes-admin' ),
							'left'   => __( 'Left', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					// array(
					// 'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
					// 'desc'    => __( 'Choose one of our predefined color skins to use with your tooltip.', 'backstop-themes-admin' ),
					// 'id'      => 'variation',
					// 'default' => '',
					// 'target'  => 'color_variations',
					// 'type'    => 'select',
					// ),
					array(
						'name' => __( 'Custom Background Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Choose your own color to use as the background for your tooltip.', 'backstop-themes-admin' ),
						'id'   => 'bgColor',
						'type' => 'color',
					),
					array(
						'name' => __( 'Custom Border Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Choose your own color to use as the border for your tooltip.', 'backstop-themes-admin' ),
						'id'   => 'borderColor',
						'type' => 'color',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can change the color of the text that appears in your tooltip.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					// array(
					// 'name'    => __( 'Sticky <small>(optional)</small>', 'backstop-themes-admin' ),
					// 'desc'    => __( 'By default the tooltip will close when you move the cursor from it.  Making it sticky will have it stay open until you hover over it again.', 'backstop-themes-admin' ),
					// 'id'      => 'sticky',
					// 'options' => array( 'true' => __( 'Tootip stays open until you hovered over', 'backstop-themes-admin' ) ),
					// 'type'    => 'checkbox',
					// ),
					// array(
					// 'name'    => __( 'Close Icon <small>(optional)</small>', 'backstop-themes-admin' ),
					// 'desc'    => __( 'Checking this will create an icon inside the tooltip when clicked on will close the tooltip.', 'backstop-themes-admin' ),
					// 'id'      => 'close',
					// 'options' => array( 'true' => __( 'Tooltip stays open until close icon is clicked', 'backstop-themes-admin' ) ),
					// 'type'    => 'checkbox',
					// ),
					// array(
					// 'name'    => __( 'Disable Arrow <small>(optional)</small>', 'backstop-themes-admin' ),
					// 'desc'    => __( 'By default there will be a small arrow on the tooltip for style purposes.  Check this if you wish to hide it.', 'backstop-themes-admin' ),
					// 'id'      => 'arrow',
					// 'options' => array( 'false' => __( 'Disable the tooltip arrow', 'backstop-themes-admin' ) ),
					// 'type'    => 'checkbox',
					// ),
						array(
							'name' => __( 'ID <small>(optional)</small>', 'backstop-themes-admin' ),
							'desc' => __( 'If using HTML for your trigger make sure to specify an ID here.', 'backstop-themes-admin' ),
							'id'   => 'custom_id',
							'type' => 'text',
						),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		global $mysite;
		$args = shortcode_atts(
			array(
				'trigger'     => '',
				'width'       => '140px',
				'position'    => 'top',
				'variation'   => '',
				'bgcolor'     => '#000000',
				'bordercolor' => '',
				'textcolor'   => '',
				'sticky'      => 'false',
				'close'       => '',
				'arrow'       => '',
				'custom_id'   => 'mysite_tooltip' . self::_tip_id(),
			),
			$atts
		);

		$mobile_disable_shortcodes = mysite_get_setting( 'mobile_disable_shortcodes' );
		if ( isset( $mysite->mobile ) && is_array( $mobile_disable_shortcodes ) && in_array( 'tooltips', $mobile_disable_shortcodes, true ) ) {
			return;
		}

		$trigger     = $args['trigger'];
		$width_num   = preg_replace( '/[^0-9]/', '', $args['width'] );
		$width       = $width_num . 'px';
		$position    = trim( $args['position'] );
		$bgcolor     = trim( $args['bgcolor'] );
		$bordercolor = trim( $args['bordercolor'] );
		$textcolor   = trim( $args['textcolor'] );
		$custom_id   = trim( $args['custom_id'] );

		$bordercolor     = $bordercolor ?: $bgcolor;
		$tt_style_prefix = ".mysite-tooltips[id='{$custom_id}']";

		$tt_style  = $tt_style_prefix . " > span{ background-color: {$bgcolor}; border: 2px solid {$bordercolor}; color: {$textcolor}; width: {$width}; }";
		$tt_style .= $tt_style_prefix . " > span:after{ border-{$position}: 8px solid {$bordercolor}; }";
		if ( 'bottom' === $position || 'top' === $position ) {
			$tt_left_margin = '-' . $width_num / 2 . 'px';
			$tt_style      .= $tt_style_prefix . ":hover > span{ margin-left: {$tt_left_margin}; }";
		}
		wp_enqueue_style( MYSITE_PREFIX . '_blank-inline-proxy', '../scripts/_blank-inline-proxy.css', array(), '0.01', 'screen' );
		wp_add_inline_style( MYSITE_PREFIX . '_blank-inline-proxy', $tt_style );

		return "<span id = '{$custom_id}' class='mysite-tooltips' mysite-tooltip-position='{$position}'>{$trigger}<span>{$content}</span></span> ";
	}

}
