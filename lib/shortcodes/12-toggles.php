<?php
/**
 * Toggles Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteToggles {

	/**
	 *
	 */
	public function toggle( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$numbers = range( 1, 10 );

			$option = array(
				'name'    => __( 'Toggle', 'backstop-themes-admin' ),
				'value'   => 'toggle',
				'options' => array(
					array(
						'name'               => __( 'Toggle 1 Title', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the title that will display with your toggle.', 'backstop-themes-admin' ),
						'id'                 => 'title',
						'default'            => '',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Toggle 1 Content', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the content that will display with your toggle.  Shortcodes are accepted.', 'backstop-themes-admin' ),
						'id'                 => 'content',
						'default'            => '',
						'type'               => 'textarea',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Toggle 1 Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'Choose one of our predefined color skins to use with your toggle.', 'backstop-themes-admin' ),
						'id'                 => 'variation',
						'default'            => '',
						'target'             => 'color_variations',
						'type'               => 'select',
						'shortcode_multiply' => true,
					),
					array(
						'name'                    => __( 'Accordion <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'                    => __( 'When using an accordian only one toggle can be opened at a time.<br /><br />When clicking on another toggle the previous one will close before opening the next.', 'backstop-themes-admin' ),
						'id'                      => 'accordion_group',
						'options'                 => array( 'true' => __( 'Group toggles into an accordion set', 'backstop-themes-admin' ) ),
						'default'                 => '',
						'type'                    => 'checkbox',
						'shortcode_dont_multiply' => true,
						'shortcode_optional_wrap' => true,
					),

					array(
						'name'                 => __( 'Total Number of Additional Toggles <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'                 => __( 'Select more toggles to display in the group.', 'backstop-themes-admin' ),
						'id'                   => 'multiply',
						'default'              => '',
						'options'              => $numbers,
						'type'                 => 'select',
						'shortcode_multiplier' => true,
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'title'     => '',
				'variation' => '',
			),
			$atts
		);

		$title     = $args['title'];
		$variation = $args['variation'];

		$variation = ( $variation ) ? ' ' . trim( $variation ) . '_sprite' : '';

		$out  = '<h6 class="toggle' . $variation . '"><a href="#">' . $title . '</a></h6>';
		$out .= '<div class="toggle_content" style="display: none;">';
		$out .= '<div class="block">';
		$out .= mysite_remove_wpautop( $content );
		$out .= '</div>';
		$out .= '</div>';

		return $out;
	}

	/**
	 *
	 */
	public function toggle_framed( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$numbers = range( 1, 10 );

			$option = array(
				'name'    => __( 'Toggle Framed', 'backstop-themes-admin' ),
				'value'   => 'toggle_framed',
				'options' => array(
					array(
						'name'               => __( 'Toggle 1 Title', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the title that will display with your toggle.', 'backstop-themes-admin' ),
						'id'                 => 'title',
						'default'            => '',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Toggle 1 Content', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the content that will display with your toggle.  Shortcodes are accepted.', 'backstop-themes-admin' ),
						'id'                 => 'content',
						'default'            => '',
						'type'               => 'textarea',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Toggle 1 Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'Choose one of our predefined color skins to use with your toggle.', 'backstop-themes-admin' ),
						'id'                 => 'variation',
						'default'            => '',
						'target'             => 'color_variations',
						'type'               => 'select',
						'shortcode_multiply' => true,
					),
					array(
						'name'                    => __( 'Accordion <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'                    => __( 'When using an accordian only one toggle can be opened at a time.<br /><br />When clicking on another toggle the previous one will close before opening the next.', 'backstop-themes-admin' ),
						'id'                      => 'accordion_group',
						'options'                 => array( 'true' => __( 'Group toggles into an accordion set', 'backstop-themes-admin' ) ),
						'default'                 => '',
						'type'                    => 'checkbox',
						'shortcode_dont_multiply' => true,
						'shortcode_optional_wrap' => true,
					),
					array(
						'name'                 => __( 'Total Number of Additional Toggles <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'                 => __( 'Select more toggles to display in the group.', 'backstop-themes-admin' ),
						'id'                   => 'multiply',
						'default'              => '',
						'options'              => $numbers,
						'type'                 => 'select',
						'shortcode_multiplier' => true,
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'title'     => '',
				'variation' => '',
			),
			$atts
		);

		$title     = $args['title'];
		$variation = $args['variation'];

		$variation = ( $variation ) ? ' ' . trim( $variation ) . '_sprite' : '';

		$out  = '<div class="toggle_frame">';
		$out .= '<h6 class="toggle' . $variation . '"><a href="#">' . $title . '</a></h6>';
		$out .= '<div class="toggle_content" style="display: none;">';
		$out .= '<div class="block">';
		$out .= mysite_remove_wpautop( $content );
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</div>';

		return $out;
	}

	/**
	 *
	 */
	public function accordion_group( $atts = null, $content = null ) {

		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Accordian Group', 'backstop-themes-admin' ),
				'value'   => 'accordion_group',
				'options' => array(
					array(
						'name'                 => __( 'Number of toggles', 'backstop-themes-admin' ),
						'desc'                 => __( 'Select how many toggles you wish to include in the accordian.', 'backstop-themes-admin' ),
						'id'                   => 'multiply',
						'default'              => '',
						'options'              => range( 1, 10 ),
						'type'                 => 'select',
						'shortcode_multiplier' => true,
					),
					array(
						'name'               => __( 'Toggle 1', 'backstop-themes-admin' ),
						'desc'               => __( 'Add a toggle to the accordian.', 'backstop-themes-admin' ),
						'id'                 => 'content',
						'default'            => '',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Toggle Title 1  <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'Add a title to the toggle.', 'backstop-themes-admin' ),
						'id'                 => 'title',
						'default'            => '',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),	
					array(
						'value'  => 'toggle',
						'nested' => true,
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$out = '';

		if ( ! preg_match_all( "/(.?)\[(toggle_framed)\b(.*?)(?:(\/))?\](?:(.+?)\[\/toggle_framed\])?(.?)/s", $content, $matches ) ) {

			if ( ! preg_match_all( "/(.?)\[(toggle)\b(.*?)(?:(\/))?\](?:(.+?)\[\/toggle\])?(.?)/s", $content, $matches ) ) {

				return mysite_remove_wpautop( $content );

			} else {

				for ( $i = 0; $i < count( $matches[0] ); $i++ ) {
					$matches[3][ $i ] = shortcode_parse_atts( $matches[3][ $i ] );
				}

				for ( $i = 0; $i < count( $matches[0] ); $i++ ) {
					$out .= '<h6 class="toggle_accordion"><a href="#">' . $matches[3][ $i ]['title'] . '</a></h6>';
					$out .= '<div class="toggle_content" style="display: none;">';
					$out .= '<div class="block">';
					$out .= mysite_remove_wpautop( $matches[5][ $i ] );
					$out .= '</div>';
					$out .= '</div>';
				}

				return '<div class="toggle_frame_set">' . $out . '</div>';
			}
		} else {

			for ( $i = 0; $i < count( $matches[0] ); $i++ ) {
				$matches[3][ $i ] = shortcode_parse_atts( $matches[3][ $i ] );
			}

			for ( $i = 0; $i < count( $matches[0] ); $i++ ) {
				$out .= '<div class="toggle_frame">';
				$out .= '<h6 class="toggle_accordion"><a href="#">' . $matches[3][ $i ]['title'] . '</a></h6>';
				$out .= '<div class="toggle_content" style="display: none;">';
				$out .= '<div class="block">';
				$out .= mysite_remove_wpautop( $matches[5][ $i ] );
				$out .= '</div>';
				$out .= '</div>';
				$out .= '</div>';
			}

			return '<div class="toggle_frame_set">' . $out . '</div>';
		}
	}

}
