<?php
/**
 * Buttons Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteButtons {

	/**
	 *
	 */
	public function button( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Button', 'backstop-themes-admin' ),
				'value'   => 'button',
				'options' => array(

					array(
						'name'    => __( 'Button Text', 'backstop-themes-admin' ),
						'desc'    => __( 'This is the text that will appear on your button.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Link Url', 'backstop-themes-admin' ),
						'desc'    => __( 'Paste a URL here to use as a link for your button.', 'backstop-themes-admin' ),
						'id'      => 'link',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Size <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'You can choose between three sizes for your button.', 'backstop-themes-admin' ),
						'id'      => 'size',
						'default' => '',
						'options' => array(
							'small'  => __( 'small', 'backstop-themes-admin' ),
							'medium' => __( 'medium', 'backstop-themes-admin' ),
							'large'  => __( 'large', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose one of our predefined color skins to use with your button.', 'backstop-themes-admin' ),
						'id'      => 'variation',
						'default' => '',
						'target'  => 'color_variations',
						'type'    => 'select',
					),
					array(
						'name' => __( 'Custom BG Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Or you can also choose your own color to use as the background for your button.', 'backstop-themes-admin' ),
						'id'   => 'bgColor',
						'type' => 'color',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can change the color of the text that appears on your button.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					array(
						'name'    => __( 'Align <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Set the alignment for your button here.<br /><br />Your button will float along the center, left or right hand sides depending on your choice.', 'backstop-themes-admin' ),
						'id'      => 'align',
						'default' => '',
						'options' => array(
							'center' => __( 'center', 'backstop-themes-admin' ),
							'left'   => __( 'left', 'backstop-themes-admin' ),
							'right'  => __( 'right', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Target <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( "Setting the target to 'Blank' will open your page in a new tab when the reader clicks on the button.", 'backstop-themes-admin' ),
						'id'      => 'target',
						'default' => '',
						'options' => array( 'blank' => __( 'Blank', 'backstop-themes-admin' ) ),
						'type'    => 'select',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'size'      => '',
				'align'     => '',
				'link'      => '#',
				'target'    => '',
				'variation' => '',
				'bgcolor'   => '',
				'textcolor' => '',
			),
			$atts
		);

		$link      = $args['link'];
		$variation = $args['variation'];
		$target    = $args['target'];
		$textcolor = $args['textcolor'];
		$bgcolor   = $args['bgcolor'];
		$size      = $args['size'];
		$align     = $args['align'];

		$size = ( $size == 'large' ) ? ' large_button' : $size;
		$size = ( $size == 'medium' ) ? ' medium_button' : $size;
		$size = ( $size == 'small' ) ? ' small_button' : $size;

		$align = ( $align ) ? ' align' . $align : '';

		$target = ( $target == 'blank' ) ? ' target_blank' : '';

		$variation = ( ( $variation ) && ( empty( $bgcolor ) ) ) ? ' ' . $variation : '';

		$styles = array();

		if ( $bgcolor ) {
			$styles[] = 'background-color:' . $bgcolor . ';border-color:' . $bgcolor . ';';
		}

		if ( $textcolor ) {
			$styles[] = 'color:' . $textcolor . ';';
		}

		$style = join( '', array_unique( $styles ) );

		$style = ! empty( $style ) ? ' style="' . $style . '"' : '';

		$out = '<a href="' . esc_url( $link ) . '" class="button_link hover_fade' . $size . $target . $align . $variation . '"' . $style . '><span>' . mysite_remove_wpautop( $content ) . '</span></a>';

			return $out;
	}

}
