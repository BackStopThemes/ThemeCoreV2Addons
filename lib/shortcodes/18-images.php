<?php
/**
 * Images Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteImages {

	public function fancy_images( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Images', 'backstop-themes-admin' ),
				'value'   => 'fancy_images',
				'options' => array(
					array(
						'name'                    => __( 'Width', 'backstop-themes-admin' ),
						'desc'                    => __( 'Set the width for your image.  Leave this blank if you do not want your image to be resized.', 'backstop-themes-admin' ),
						'id'                      => 'width',
						'type'                    => 'text',
						'shortcode_dont_multiply' => true,
					),
					array(
						'name'                    => __( 'Height', 'backstop-themes-admin' ),
						'desc'                    => __( 'Set the width for your image.  Leave this blank if you do not want your image to be resized.', 'backstop-themes-admin' ),
						'id'                      => 'height',
						'type'                    => 'text',
						'shortcode_dont_multiply' => true,
					),
					array(
						'name'                 => __( 'Number of images', 'backstop-themes-admin' ),
						'desc'                 => __( 'Select how many images you wish to display.', 'backstop-themes-admin' ),
						'id'                   => 'multiply',
						'options'              => range( 1, 20 ),
						'type'                 => 'select',
						'shortcode_multiplier' => true,
					),
					array(
						'name'               => __( 'Image 1 URL', 'backstop-themes-admin' ),
						'desc'               => __( 'You can upload the image you wish to use here.', 'backstop-themes-admin' ),
						'id'                 => 'content',
						'type'               => 'upload',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Image 1 Title Attribute <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the title text you wish to use with your image.', 'backstop-themes-admin' ),
						'id'                 => 'title',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Image 1 Alt Attribute <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the alt text you wish to use with your image.', 'backstop-themes-admin' ),
						'id'                 => 'alt',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Image 1 Caption <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'Type out the caption text you wish to use with your image.<br /><br />This text will be displayed below your image.', 'backstop-themes-admin' ),
						'id'                 => 'caption',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'name'               => __( 'Image 1 Custom Link <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'               => __( 'By default when a reader clicks on your image it will open in a lightbox.<br /><br />You can paste a URL here to use instead.', 'backstop-themes-admin' ),
						'id'                 => 'link_to',
						'type'               => 'text',
						'shortcode_multiply' => true,
					),
					array(
						'value'  => 'image',
						'nested' => true,
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		global $mysite;

		$args = shortcode_atts(
			array(
				'width'  => '',
				'height' => '',
				'group'  => '',
				'class'  => 'true',
			),
			$atts
		);

		$width  = $args['width'];
		$height = $args['height'];
		$group  = $args['group'];
		$class  = $args['class'];

		$out = '';

		$width  = ( ! empty( $width ) ) ? trim( str_replace( ' ', '', str_replace( 'px', '', $width ) ) ) : $mysite->layout['images']['three_column_portfolio'][0];
		$height = ( ! empty( $height ) ) ? trim( str_replace( ' ', '', str_replace( 'px', '', $height ) ) ) : $mysite->layout['images']['three_column_portfolio'][1];

		if ( ! preg_match_all( '/(.?)\[(image)\b(.*?)(?:(\/))?\](?:(.+?)\[\/image\])?(.?)/s', $content, $matches ) ) {

			if ( preg_match_all( '!.+\.(?:jpe?g|png|gif)!Ui', $content, $matches ) ) {

				if ( empty( $group ) ) {
					$group = 'fancy_img_group_' . rand( 1, 1000 );
				}

				$out .= '<div class="fancy_images">';

				foreach ( (array) ( $matches[0] ) as $img ) {
					$out .= '<div class="fancy_image">';

					$out .= mysite_display_image(
						array(
							'src'         => $img,
							'alt'         => '',
							'title'       => '',
							'height'      => $height,
							'width'       => $width,
							'class'       => ( $class == 'true' ? 'hover_fade_js' : '' ),
							'link_to'     => $img,
							'link_class'  => 'fancy_image_load',
							'prettyphoto' => true,
							'group'       => $group,
							'preload'     => ( isset( $mysite->mobile ) ? false : true ),
						)
					);

					$out .= '</div>';
				}
				$out .= '</div>';
			}
		} else {

			for ( $i = 0; $i < count( $matches[0] ); $i++ ) {

				$matches[3][ $i ] = str_replace( '&#8221;', '"', $matches[3][ $i ] );

				$matches[3][ $i ] = shortcode_parse_atts( $matches[3][ $i ] );
				if ( isset( $matches[3][ $i ]['caption'] ) ) {
					$has_caption = true;
				}
			}

			if ( empty( $group ) ) {
				$group = 'fancy_img_group_' . rand( 1, 1000 );
			}

			$out .= '<div class="fancy_images' . ( isset( $has_caption ) ? ' has_captions' : '' ) . '">';

			for ( $i = 0; $i < count( $matches[0] ); $i++ ) {

				$img   = $matches[5][ $i ];
				$alt   = ( isset( $matches[3][ $i ]['alt'] ) ) ? $matches[3][ $i ]['alt'] : '';
				$title = ( isset( $matches[3][ $i ]['title'] ) ) ? $matches[3][ $i ]['title'] : '';

				$link_to     = ( ! empty( $matches[3][ $i ]['link_to'] ) ) ? $matches[3][ $i ]['link_to'] : $img;
				$prettyphoto = ( ( ! empty( $matches[3][ $i ]['link_to'] ) ) && ( strpos( $matches[3][ $i ]['link_to'], 'iframe' ) === false ) ) ? false : true;

				$out .= '<div class="fancy_image">';
				$out .= mysite_display_image(
					array(
						'src'         => $img,
						'alt'         => $alt,
						'title'       => $title,
						'height'      => $height,
						'width'       => $width,
						'class'       => ( $class == 'true' ? 'hover_fade_js' : '' ),
						'link_to'     => $link_to,
						'link_class'  => 'fancy_image_load',
						'prettyphoto' => $prettyphoto,
						'group'       => $group,
						'preload'     => ( isset( $mysite->mobile ) ? false : true ),
					)
				);

				if ( isset( $has_caption ) ) {
					$out .= '<span class="fancy_image_caption" style="display:none;">' . $matches[3][ $i ]['caption'] . '</span>';
				}

				$out .= '</div>';
			}
			$out .= '</div>';
		}

		return $out;
	}

}
