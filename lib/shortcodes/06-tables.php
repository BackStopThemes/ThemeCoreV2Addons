<?php
/**
 * Table Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteTables {

	/**
	 *
	 */
	public function fancy_table( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'                      => __( 'Tables', 'backstop-themes-admin' ),
				'value'                     => 'fancy_table',
				'options'                   => array(
					'name'    => __( 'Table Html', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content of your table.  You need to use the HTML table tags when typing out your content.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
				'shortcode_carriage_return' => true,
			);

			return $option;
		}

		return str_replace( '<table>', '<table class="fancy_table">', mysite_remove_wpautop( $content ) );
	}

	/**
	 *
	 */
	public function minimal_table( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'                      => __( 'Minimal Table', 'backstop-themes-admin' ),
				'value'                     => 'minimal_table',
				'options'                   => array(
					'name'    => __( 'Table Html', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content of your table.  You need to use the HTML table tags when typing out your content.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
				'shortcode_carriage_return' => true,
			);

			return $option;
		}

		return str_replace( '<table>', '<table class="minimal_table">', mysite_remove_wpautop( $content ) );
	}

}
