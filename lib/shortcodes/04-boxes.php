<?php
/**
 * Boxes Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteBoxes {

	/**
	 *
	 */
	public function download_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Download Box', 'backstop-themes-admin' ),
				'value'   => 'download_box',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		return '<div class="download_box">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function warning_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Warning Box', 'backstop-themes-admin' ),
				'value'   => 'warning_box',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		return '<div class="warning_box">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function info_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Info Box', 'backstop-themes-admin' ),
				'value'   => 'info_box',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		return '<div class="info_box">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function note_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Note Box', 'backstop-themes-admin' ),
				'value'   => 'note_box',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		return '<div class="note_box">' . mysite_remove_wpautop( $content ) . '</div>';
	}

	/**
	 *
	 */
	public function fancy_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Fancy Box', 'backstop-themes-admin' ),
				'value'   => 'fancy_box',
				'options' => array(
					array(
						'name'    => __( 'Box Content', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'textarea',
					),
					array(
						'name'    => __( 'Title <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out a title to use with your fancy box.  The title will display above the content.', 'backstop-themes-admin' ),
						'id'      => 'title',
						'default' => '',
						'type'    => 'text',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'title' => '',
			),
			$atts
		);

		$title = $args['title'];

		$out = '<div class="fancy_box">';

		if ( ! empty( $title ) ) {
			$out .= '<h6 class="fancy_box_title"><span>' . $title . '</span></h6>';
		}

		$out .= '<div class="fancy_box_content">' . mysite_remove_wpautop( $content ) . '</div>';
		$out .= '</div>';

		return $out;
	}

	/**
	 *
	 */
	public function titled_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Titled Box', 'backstop-themes-admin' ),
				'value'   => 'titled_box',
				'options' => array(
					array(
						'name'    => __( 'Box Title', 'backstop-themes-admin' ),
						'desc'    => __( 'Type out the title to use with your titled box.  The title will display above the content.', 'backstop-themes-admin' ),
						'id'      => 'title',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name' => __( 'Box Content', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
						'id'   => 'content',
						'type' => 'textarea',
					),
					array(
						'name'   => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'   => __( 'Choose one of our predefined color skins to use with your box.', 'backstop-themes-admin' ),
						'id'     => 'variation',
						'target' => 'color_variations',
						'type'   => 'select',
					),
					array(
						'name' => __( 'Custom BG Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Or you can also choose your own color to use as the background for your box.', 'backstop-themes-admin' ),
						'id'   => 'bgColor',
						'type' => 'color',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can change the color of the text that appears on your box.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'title'     => '',
				'variation' => '',
				'bgcolor'   => '',
				'textcolor' => '',
			),
			$atts
		);

		$title     = $args['title'];
		$variation = $args['variation'];
		$bgcolor   = $args['bgcolor'];
		$textcolor = $args['textcolor'];

		$variation = ( ( $variation ) && ( empty( $bgcolor ) ) ) ? ' ' . $variation : '';

		$styles = array();

		if ( $bgcolor ) {
			$styles[] = 'background-color:' . $bgcolor . ';border-color:' . $bgcolor . ';';
		}

		if ( $textcolor ) {
			$styles[] = 'color:' . $textcolor . ';';
		}

		$style = join( '', array_unique( $styles ) );

		$style = ( ! empty( $style ) ) ? ' style="' . $style . '"' : '';

		$out  = '<div class="titled_box">';
		$out .= '<h6 class="titled_box_title' . $variation . '"' . $style . '><span>' . $title . '</span></h6>';
		$out .= '<div class="titled_box_content">' . mysite_remove_wpautop( $content ) . '</div>';
		$out .= '</div>';

		return $out;
	}

	/**
	 *
	 */
	public function colored_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Colored Box', 'backstop-themes-admin' ),
				'value'   => 'colored_box',
				'options' => array(
					array(
						'name' => __( 'Box Content', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
						'id'   => 'content',
						'type' => 'textarea',
					),
					array(
						'name' => __( 'Box Title <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the title to use with your titled box.  The title will display above the content.', 'backstop-themes-admin' ),
						'id'   => 'title',
						'type' => 'text',
					),

					array(
						'name'   => __( 'Color Variation <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'   => __( 'Choose one of our predefined color skins to use with your box.', 'backstop-themes-admin' ),
						'id'     => 'variation',
						'target' => 'color_variations',
						'type'   => 'select',
					),
					array(
						'name' => __( 'Custom BG Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'Or you can also choose your own color to use as the background for your box.', 'backstop-themes-admin' ),
						'id'   => 'bgColor',
						'type' => 'color',
					),
					array(
						'name' => __( 'Custom Text Color <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'You can change the color of the text that appears on your box.', 'backstop-themes-admin' ),
						'id'   => 'textColor',
						'type' => 'color',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'title'     => '',
				'variation' => '',
				'bgcolor'   => '',
				'textcolor' => ''
			),
			$atts
		);

		$title     = $args['title'];
		$variation = $args['variation'];
		$bgcolor   = $args['bgcolor'];
		$textcolor = $args['textcolor'];

		$variation = ( ( $variation ) && ( empty( $bgcolor ) ) ) ? ' ' . $variation : '';

		$styles = array();

		if ( $bgcolor ) {
			$styles[] = 'background-color:' . $bgcolor . ';border-color:' . $bgcolor . ';';
		}

		if ( $textcolor ) {
			$styles[] = 'color:' . $textcolor . ';';
		}

		$style = join( '', array_unique( $styles ) );

		$style = ( ! empty( $style ) ) ? ' style="' . $style . '"' : '';

		$out = '<div class="colored_box' . $variation . '"' . $style . '>';

		if ( ! empty( $title ) ) {
			$out .= '<h6 class="colored_box_title"><span>' . $title . '</span></h6>';
		}

		$out .= '<div class="colored_box_content">' . mysite_remove_wpautop( $content ) . '</div>';
		$out .= '</div>';

		return $out;
	}

	/**
	 *
	 */
	public function fancy_code_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Code Box', 'backstop-themes-admin' ),
				'value'   => 'fancy_code_box',
				'options' => array(
					'name'    => 'Box Content',
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		return '<pre class="fancy_code_box">' . mysite_remove_wpautop( $content ) . '</pre>';
	}

	/**
	 *
	 */
	public function fancy_pre_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Pre Box', 'backstop-themes-admin' ),
				'value'   => 'fancy_pre_box',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		return '<pre class="fancy_pre_box">' . mysite_remove_wpautop( $content ) . '</pre>';
	}

	/**
	 *
	 */
	public function squeeze_box( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Squeeze Box 1', 'backstop-themes-admin' ),
				'value'   => 'squeeze_box',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = " style='" . $css . "'";
		}

		if ( ! empty( $classes ) ) {
			 $classes = ' ' . $classes;
		}

		return '<div class="squeeze_box' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</div>';
	}

	/**
	 *
	 */
	public function squeeze_box2( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Squeeze Box 2', 'backstop-themes-admin' ),
				'value'   => 'squeeze_box2',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = " style='" . $css . "'";
		}

		if ( ! empty( $classes ) ) {
			 $classes = ' ' . $classes;
		}

		return '<div class="squeeze_box2' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</div>';
	}

	/**
	 *
	 */
	public function squeeze_box3( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Squeeze Box 3', 'backstop-themes-admin' ),
				'value'   => 'squeeze_box3',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = " style='" . $css . "'";
		}

		if ( ! empty( $classes ) ) {
			 $classes = ' ' . $classes;
		}

		return '<div class="squeeze_box3' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</div>';
	}

	/**
	 *
	 */
	public function squeeze_box4( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Squeeze Box 4', 'backstop-themes-admin' ),
				'value'   => 'squeeze_box4',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = " style='" . $css . "'";
		}

		if ( ! empty( $classes ) ) {
			 $classes = ' ' . $classes;
		}

		return '<div class="squeeze_box4' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</div>';
	}

	/**
	 *
	 */
	public function squeeze_box5( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Squeeze Box 5', 'backstop-themes-admin' ),
				'value'   => 'squeeze_box5',
				'options' => array(
					'name'    => __( 'Box Content', 'backstop-themes-admin' ),
					'desc'    => __( 'Type out the content that you wish to display inside your box.', 'backstop-themes-admin' ),
					'id'      => 'content',
					'default' => '',
					'type'    => 'textarea',
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'css'     => '',
				'classes' => '',
			),
			$atts
		);

		$css     = $args['css'];
		$classes = $args['classes'];

		if ( ! empty( $css ) ) {
			$css = " style='" . $css . "'";
		}

		if ( ! empty( $classes ) ) {
			$classes = ' ' . $classes;
		}

		return '<div class="squeeze_box5' . $classes . '"' . $css . '>' . mysite_remove_wpautop( do_shortcode( $content ) ) . '</div>';
	}

}
