<?php
/**
 * Frames Shortcodes
 *
 * @package BackStopThemes
 * @subpackage Shortcodes
 */

/**
 * Undocumented class
 */
class mysiteFrames {

	/**
	 *
	 */
	public function image_frame( $atts = null, $content = null ) {
		if ( 'generator' === $atts ) {
			$option = array(
				'name'    => __( 'Frames', 'backstop-themes-admin' ),
				'value'   => 'image_frame',
				'options' => array(
					array(
						'name'    => __( 'Type', 'backstop-themes-admin' ),
						'desc'    => __( 'Choose which type of frame you wish to use.', 'backstop-themes-admin' ),
						'id'      => 'style',
						'default' => '',
						'options' => array(
							'border'         => __( 'Transparent Border', 'backstop-themes-admin' ),
							'reflect'        => __( 'Reflection', 'backstop-themes-admin' ),
							'framed'         => __( 'Framed', 'backstop-themes-admin' ),
							'shadow'         => __( 'Shadow', 'backstop-themes-admin' ),
							'reflect_shadow' => __( 'Reflection + Shadow', 'backstop-themes-admin' ),
							'framed_shadow'  => __( 'Framed + Shadow', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Image URL', 'backstop-themes-admin' ),
						'desc'    => __( 'You can upload your image that you wish to use here.', 'backstop-themes-admin' ),
						'id'      => 'content',
						'default' => '',
						'type'    => 'upload',
					),
					array(
						'name'    => __( 'Align <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Set the alignment for your image here.<br /><br />Your image will float along the center, left or right hand sides depending on your choice.', 'backstop-themes-admin' ),
						'id'      => 'align',
						'default' => '',
						'options' => array(
							'left'   => __( 'left', 'backstop-themes-admin' ),
							'right'  => __( 'right', 'backstop-themes-admin' ),
							'center' => __( 'center', 'backstop-themes-admin' ),
						),
						'type'    => 'select',
					),
					array(
						'name'    => __( 'Alt Attribute <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Type the alt text that you would like to display with your image here.', 'backstop-themes-admin' ),
						'id'      => 'alt',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Title Attribute <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'Type the title text that you would like to display with your image here.', 'backstop-themes-admin' ),
						'id'      => 'title',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Image Height <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'You can set the image height here.  Leave this blank if you do not want to resize your image.', 'backstop-themes-admin' ),
						'id'      => 'height',
						'default' => '',
						'type'    => 'text',
					),
					array(
						'name'    => __( 'Image Width <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc'    => __( 'You can set the image width here.  Leave this blank if you do not want to resize your image.', 'backstop-themes-admin' ),
						'id'      => 'width',
						'default' => '',
						'type'    => 'text',
					),
					'shortcode_has_atts' => true,
				),
			);

			return $option;
		}

		$args = shortcode_atts(
			array(
				'style'       => '',
				'align'       => '',
				'alt'         => '',
				'title'       => '',
				'height'      => '',
				'width'       => '',
				'link_to'     => 'true',
				'prettyphoto' => 'true',
			),
			$atts
		);

		$style       = $args['style'];
		$align       = $args['align'];
		$alt         = $args['alt'];
		$title       = $args['title'];
		$height      = $args['height'];
		$width       = $args['width'];
		$link_to     = $args['link_to'];
		$prettyphoto = $args['prettyphoto'];

		$out = '';

		$effect = trim( $style );
		$effect = ( ! empty( $effect ) ) ? $effect : 'framed';
		$align  = ( 'left' === $align ? ' alignleft' : ( 'right' === $align ? ' alignright' : ( 'center' === $align ? ' aligncenter' : ' alignleft' ) ) );
		$class  = ( 'reflect' === $effect ? "reflect{$align}" : ( 'reflect_shadow' === $effect ? 'reflect' : ( 'framed' === $effect ? "framed{$align}" : ( 'framed_shadow' === $effect ? 'framed' : '' ) ) ) );

		$width  = ( ! empty( $width ) ) ? trim( str_replace( ' ', '', str_replace( 'px', '', $width ) ) ) : '';
		$height = ( ! empty( $height ) ) ? trim( str_replace( ' ', '', str_replace( 'px', '', $height ) ) ) : '';

		if ( preg_match( '!.+\.(?:jpe?g|png|gif)!Ui', $content, $matches ) ) {
			$out .= mysite_display_image(
				array(
					'src'         => $content,
					'alt'         => $alt,
					'title'       => $title,
					'class'       => $class,
					'height'      => $height,
					'width'       => $width,
					'link_to'     => ( 'true' === $link_to ? $matches[0] : false ),
					'prettyphoto' => ( 'true' === $prettyphoto ? true : false ),
					'align'       => $align,
					'effect'      => $effect,
					'wp_resize'   => ! mysite_get_setting( 'image_resize' ),
				)
			);
		}

		return $out;
	}

}
