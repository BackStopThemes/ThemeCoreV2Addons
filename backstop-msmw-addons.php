<?php
/**
 * Plugin Name: BackStop-MSMW-Addons
 * Plugin URI: https://backstopthemes.com
 * Version: 1.0
 * Author: OnePressTech Pty Ltd
 * Description: BackStop Themes Addons
 * Text Domain: backstop-themes
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0-standalone.html
 *
 * @package BackStopThemes
 * @subpackage Functions
 */

// This plug-in does not process heartbeat AJAX calls so ignore them. NOTE: Nonce check is not being used because it can fail with an error "Call to undefined function wp_verify_nonce" and we can't defer until later.
if ( isset( $_REQUEST['action'] ) && ( 'heartbeat' === $_REQUEST['action'] ) ) {
	return;
}
// Check for minimum required PHP version.
define( 'PLUGIN_REQUIRED_PHP_VERSION', '5.6.0' );
if ( version_compare( phpversion(), PLUGIN_REQUIRED_PHP_VERSION, '<' ) ) {
	add_action( 'admin_notices', 'bst_addons_admin_notice' );
	/**
	 * Display PHP version mismatch error message.
	 *
	 * @return void.
	 */
	function bst_addons_admin_notice() {
		?>
		<div class="notice notice-error is-dismissible">
		<p><?php esc_html_e( 'BackStop Themes require that your PHP version be upgraded from your current version ', 'backstop-themes' ); ?> <strong><?php echo phpversion(); ?></strong> <?php _e( ' to at least version ', 'backstop-themes' ); ?> <strong><?php echo PLUGIN_REQUIRED_PHP_VERSION; ?></strong></p>
		</div>
		<?php
	}
} else { // Get theme data and Initialize the theme.

	$bst_theme_data = wp_get_theme();
	if ( $bst_theme_data->exists() && ( 'backstop-msmw' === $bst_theme_data->template ) ) {

		// Switch the theme's template directory to this plugin's template directory.
		define( 'TEMPLATEPATH', rtrim( plugin_dir_path( __FILE__ ), '/' ) );

		// Load the BackStopThemes class.
		require_once 'themecore-library.php';
		require_once 'themecore.php';

		( new BackStopThemes() )->init(
			array(
				'theme_name'    => bst_get_theme_name(),
				'theme_version' => bst_get_theme_version(),
			)
		);
	}
}
