# BackStop-MSMW Theme

| Documentation Index             | Details                                                                                                     |
| --------------------------------|-------------------------------------------------------------------------------------------------------------|
| **[General Issue Forum][1]**    | A community Question & Answer Forum for general issues related to the BackStopThemes initiative             |
| **[Overview][2]**               | One page overview of the vision, content and structure of the BackStop Themes Suite (this page)             |
| **[Development History][3]**    | A brief history of the evolution of BackStop Themes                                                         |

[1]: https://gitlab.com/BackStopThemes/GeneralIssuesForum/
[2]: https://gitlab.com/BackStopThemes/ThemeCoreV2/
[3]: https://gitlab.com/BackStopThemes/ThemeCoreV2/wikis/development-history/

------------------------------------------------------------------------------------------------------------------------------------------

## Theme Design Overview

The BackStop Themes MSMW Edition is a combination of this base theme and one of the theme-specific BST-MSMW plug-ins.

------------------------------------------------------------------------------------------------------------------------------------------

### Theme Build & Installation Instructions

To build this theme simply download the files in this project, remove the git directories (optional) and combine them to form a single folder on your computer named backstop-msmw. Then, Zip the folder, rename it to BackStop-MSMW-Vx.y.z.zip (optional) and follow [the standard WordPress theme installation instructions](https://codex.wordpress.org/Using_Themes).

------------------------------------------------------------------------------------------------------------------------------------------

### License

GPLv2 (or later)

------------------------------------------------------------------------------------------------------------------------------------------

### Terms & Conditions

THE SERVICES ARE PROVIDED AS IS WITHOUT ANY REPRESENTATION OR WARRANTY OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE WITH RESPECT TO THE SERVICE. EXCEPT TO THE EXTENT PROHIBITED BY LAW, WE DISCLAIM ALL WARRANTIES INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR PURPOSE AND NON-INFRINGEMENT.
NEITHER WE NOR OUR LICENSORS SHALL BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES INCLUDING BUT NOT LIMITED TO DAMAGES FOR LOSS OF PROFITS OR GOODWILL EVEN IF SUCH LOSS OR DAMAGE WAS REASONABLY FORSEEABLE.

By forking this software you agree to these terms & conditions. These terms may be adjusted from time to time so please check here periodically for any updates. If you do not agree to the modified terms, you should discontinue your use of this software.

------------------------------------------------------------------------------------------------------------------------------------------

### Credits

Webtreats / MySiteMyWay is the founding creator of this open source theme software forked to create BackStop Themes.

The SkyBind division of OnePressTech Pty Ltd. is the Founder of BackStop Themes and the manager of the BackStop Themes software evolution along with the BackStop Themes community. OnePressTech is a website and Managed Office Cloud provider to micro-businesses, NGOs, community groups, and small departments in small, mid-sized and large organsisations.

For the back-story [click here](https://gitlab.com/BackStopThemes/ThemeCoreV2/wikis/development-history/).
